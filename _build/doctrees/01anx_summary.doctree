���S      �docutils.nodes��document���)��}�(�symbol_footnote_refs�]��children�]�(h �target���)��}�(�line�Khh�source��7/home/likicv/anx-docs/anx-docs/source/01anx_summary.rst��tagname�h	�	rawsource��.. _introduction:�h]��parent�h�
attributes�}�(�refid��introduction��ids�]��backrefs�]��names�]��dupnames�]��classes�]�uubh �section���)��}�(�expect_referenced_by_id�}�hhshKhh�expect_referenced_by_name�}��introduction�hshhhh#h� �h]�(h �title���)��}�(hKhhhhhh.h�Summary�h]�h �Text����Summary�����}�(hhhNhh2hNhh0ubahh%h}�(h]�h]�h]�h!]�h]�uubh �
field_list���)��}�(hK	hhhhhh@hh,h]�(h �field���)��}�(hK	hhhhhhEhh,h]�(h �
field_name���)��}�(hK hhhhJh�
Last Built�h]�h5�
Last Built�����}�(hhNhhLubahhGh}�(h]�h]�h]�h!]�h]�uubh �
field_body���)��}�(hhZh�|today|�h]�h �	paragraph���)��}�(hK	hhhh`hh^h]�h5�Aug 23, 2017�����}�(h�Aug 23, 2017�hhbubahh\h}�(h]�h]�h]�h!]�h]�uubahhGh}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubhF)��}�(hK
hhhhhhEhh,h]�(hK)��}�(hK hhhhJh�Page Status�h]�h5�Page Status�����}�(hh�hhubahh|h}�(h]�h]�h]�h!]�h]�uubh[)��}�(hhZh�Incomplete
�h]�ha)��}�(hK
hhhh`h�
Incomplete�h]�h5�
Incomplete�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubahh|h}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]�h]�h]�h!]�h]�uubh �topic���)��}�(hKhhhhhh�hh,h]�(h/)��}�(hh.h�Contents�h]�h5�Contents�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubh �bullet_list���)��}�(hNhhhNhh�hh,h]�(h �	list_item���)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h �	reference���)��}�(hh�hh,h]�h5�Introduction�����}�(h�Introduction�hh�ubahh�h}�(�refid��index-0�h]��id2�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h�)��}�(hh�hh,h]�h5�Implementation�����}�(h�Implementation�hh�ubahh�h}�(�refid��implementation�h]��id3�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h�)��}�(hh�hh,h]�h5�How is this guide organised�����}�(h�How is this guide organised�hj  ubahj  h}�(�refid��how-is-this-guide-organised�h]��id4�ah]�h]�h]�h!]�uubahj  h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubehh�h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��contents�ah]��contents�ah]�h!]�(�contents��local�eh]�uub�sphinx.addnodes��index���)��}�(hKhhhhhjG  hh,h]�hh%h}�(h]�h]��inline��h]�h]�h!]��entries�]�(�single��summary description�h�h,Nt�auubh
)��}�(hKhhhhhh	hh,h]�hh%h}�(hh�h]�h]�h]�h]�h!]�uubh$)��}�(h'}�h�jX  shKhhh)}�hhhh#hh,h]�(h/)��}�(hKhhhhhh.hh�h]�h5�Introduction�����}�(hNhh�hNhhhjf  ubahja  h}�(hh�h]�h]�h]�h]�h!]�uubha)��}�(hKhhhhhh`hX  Abstract Network Simulator (``anx``) is a generic discrete-event
network simulator for networks that exchange discrete information
packets. ``anx`` allows one to investigate network dynamics in
communication networks and social networks, where they can be
approximated as packet-based information exchange networks.
``anx`` aims for a high level of abstraction, and allows one to
study dynamics of networks of arbitrary size and topology.
``anx`` also provides a simulation framework that can be easily
extended to model specific behaviours.�h]�(h5�Abstract Network Simulator (�����}�(hNh�Abstract Network Simulator (�hNhhhjs  ubh �literal���)��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj~  ubahjs  h}�(h]�h]�h]�h!]�h]�uubh5�i) is a generic discrete-event
network simulator for networks that exchange discrete information
packets. �����}�(hNh�i) is a generic discrete-event
network simulator for networks that exchange discrete information
packets. �hNhhhjs  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahjs  h}�(h]�h]�h]�h!]�h]�uubh5�� allows one to investigate network dynamics in
communication networks and social networks, where they can be
approximated as packet-based information exchange networks.
�����}�(hNh�� allows one to investigate network dynamics in
communication networks and social networks, where they can be
approximated as packet-based information exchange networks.
�hNhhhjs  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahjs  h}�(h]�h]�h]�h!]�h]�uubh5�t aims for a high level of abstraction, and allows one to
study dynamics of networks of arbitrary size and topology.
�����}�(hNh�t aims for a high level of abstraction, and allows one to
study dynamics of networks of arbitrary size and topology.
�hNhhhjs  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahjs  h}�(h]�h]�h]�h!]�h]�uubh5�_ also provides a simulation framework that can be easily
extended to model specific behaviours.�����}�(hNh�_ also provides a simulation framework that can be easily
extended to model specific behaviours.�hNhhhjs  ubehja  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hK hhhhhh`hXZ  ``anx`` is primarily a research and education tool. It may be of
interest to researchers interested in a specific problem related
to network dynamics, and  to anyone learning about network
simulations. ``anx`` is written in Python, a high-level programming
language widely used in science and network research. For more
details see :ref:`python`.�h]�(j}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh5�� is primarily a research and education tool. It may be of
interest to researchers interested in a specific problem related
to network dynamics, and  to anyone learning about network
simulations. �����}�(hNh�� is primarily a research and education tool. It may be of
interest to researchers interested in a specific problem related
to network dynamics, and  to anyone learning about network
simulations. �hNhhhj�  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh5�{ is written in Python, a high-level programming
language widely used in science and network research. For more
details see �����}�(hNh�{ is written in Python, a high-level programming
language widely used in science and network research. For more
details see �hNhhhj�  ubjF  �pending_xref���)��}�(hK hhhj�  h�:ref:`python`�h]�h jO  ��)��}�(hjO  hj�  h]�h5�python�����}�(hh,hj  ubahj�  h}�(h]�h]�h]�h!]�(�xref��std��std-ref�eh]�uubahj�  h}�(�refdoc��01anx_summary��refexplicit��h]�h]��reftype��ref��	reftarget��python��refwarn��h]�h]�h!]��	refdomain�j  uubh5�.�����}�(hNh�.�hNhhhj�  ubehja  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]�h]�(h݌id1�eh]��introduction�ah!]�h]�u�
referenced�Kubh$)��}�(hK)hhhhhh#hh,h]�(h/)��}�(hK)hhhhhh.hh�h]�h5�Implementation�����}�(hNhh�hNhhhj7  ubahj4  h}�(hj  h]�h]�h]�h]�h!]�uubha)��}�(hK+hhhhhh`hX=  ``anx`` is written in pure Python3, and has two core dependencies
outside Python standard libraries: |NetworkX| and |SimPy|.
NetworkX is a library for creation and manipulation of complex
networks, where these are modeled, viewed and manipulated as
graphs. SimPy is a general library for discrete-event simulations
framework that relies on Python generators (first introduced in
Python 2.5). ``anx`` uses NetworkX to create and manipulate network
representations, and once these are converted into a computational
model and uses SimPy for the simulation of network traffic.�h]�(j}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hjH  ubahjD  h}�(h]�h]�h]�h!]�h]�uubh5�^ is written in pure Python3, and has two core dependencies
outside Python standard libraries: �����}�(hNh�^ is written in pure Python3, and has two core dependencies
outside Python standard libraries: �hNhhhjD  ubh �raw���)��}�(hKUhhhhhj[  h�A<a href="https://networkx.github.io" target="_blank">NetworkX</a>�h]�h5�A<a href="https://networkx.github.io" target="_blank">NetworkX</a>�����}�(hNhh,hNhhhj]  ubahjD  h}�(h]�h]��	xml:space��preserve��format��html�h]�h]�h!]�uubh5� and �����}�(hNh� and �hNhhhjD  ubj\  )��}�(hKYhhhhhj[  h�J<a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>�h]�h5�J<a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>�����}�(hNhh,hNhhhjt  ubahjD  h}�(h]�h]��	xml:space�ji  �format��html�h]�h]�h!]�uubh5X  .
NetworkX is a library for creation and manipulation of complex
networks, where these are modeled, viewed and manipulated as
graphs. SimPy is a general library for discrete-event simulations
framework that relies on Python generators (first introduced in
Python 2.5). �����}�(hNhX  .
NetworkX is a library for creation and manipulation of complex
networks, where these are modeled, viewed and manipulated as
graphs. SimPy is a general library for discrete-event simulations
framework that relies on Python generators (first introduced in
Python 2.5). �hNhhhjD  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahjD  h}�(h]�h]�h]�h!]�h]�uubh5�� uses NetworkX to create and manipulate network
representations, and once these are converted into a computational
model and uses SimPy for the simulation of network traffic.�����}�(hNh�� uses NetworkX to create and manipulate network
representations, and once these are converted into a computational
model and uses SimPy for the simulation of network traffic.�hNhhhjD  ubehj4  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��implementation�ah]�j   ah]�h!]�h]�uubh$)��}�(hK7hhhhhh#hh,h]�(h/)��}�(hK7hhhhhh.hj   h]�h5�How is this guide organised�����}�(hNhj   hNhhhj�  ubahj�  h}�(hj%  h]�h]�h]�h]�h!]�uubha)��}�(hK9hhhhhh`h��This guide is organised into a number of sections. The next section
shows the quick installation instructions (:ref:`installation`).
The following sections are a general discussion on modelling and how
it applies to network simulations:�h]�(h5�oThis guide is organised into a number of sections. The next section
shows the quick installation instructions (�����}�(hNh�oThis guide is organised into a number of sections. The next section
shows the quick installation instructions (�hNhhhj�  ubj�  )��}�(hK9hhhj�  h�:ref:`installation`�h]�j   )��}�(hjO  hj�  h]�h5�installation�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �installation�j  �h]�h]�h!]��	refdomain�j�  uubh5�j).
The following sections are a general discussion on modelling and how
it applies to network simulations:�����}�(hNh�j).
The following sections are a general discussion on modelling and how
it applies to network simulations:�hNhhhj�  ubehj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hK>hhhhhh�hh,h]�(h�)��}�(hNhhhhhh�h�:ref:`models`�h]�ha)��}�(hK>hhhh`hj�  h]�j�  )��}�(hK>hhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�models�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �models�j  �h]�h]�h!]��	refdomain�j  uubahj�  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`level_of_abstraction`�h]�ha)��}�(hK?hhhh`hj!  h]�j�  )��}�(hK?hhhj�  hj!  h]�j   )��}�(hjO  hj!  h]�h5�level_of_abstraction�����}�(hh,hj)  ubahj&  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj#  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �level_of_abstraction�j  �h]�h]�h!]��	refdomain�j5  uubahj  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`network_simulation`�h]�ha)��}�(hK@hhhh`hjQ  h]�j�  )��}�(hK@hhhj�  hjQ  h]�j   )��}�(hjO  hjQ  h]�h5�network_simulation�����}�(hh,hjY  ubahjV  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahjS  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �network_simulation�j  �h]�h]�h!]��	refdomain�je  uubahjO  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�!:ref:`discrete_event_simulations`�h]�ha)��}�(hKAhhhh`hj�  h]�j�  )��}�(hKAhhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�discrete_event_simulations�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �discrete_event_simulations�j  �h]�h]�h!]��	refdomain�j�  uubahj  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`python`

�h]�ha)��}�(hKBhhhh`h�:ref:`python`�h]�j�  )��}�(hKBhhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�python�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �python�j  �h]�h]�h!]��	refdomain�j�  uubahj�  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubehj�  h}�(h]�h]��bullet��*�h]�h]�h!]�uubha)��}�(hKEhhhhhh`h��These sections are not specific to ``anx`` simulator. If you are interested
to get a quick perspective on how to start working with the ``anx`` simulator,
jump here: :ref:`first_tutorial`.�h]�(h5�#These sections are not specific to �����}�(hNh�#These sections are not specific to �hNhhhj�  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh5�^ simulator. If you are interested
to get a quick perspective on how to start working with the �����}�(hNh�^ simulator. If you are interested
to get a quick perspective on how to start working with the �hNhhhj�  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hj  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh5� simulator,
jump here: �����}�(hNh� simulator,
jump here: �hNhhhj�  ubj�  )��}�(hKEhhhj�  h�:ref:`first_tutorial`�h]�j   )��}�(hjO  hj  h]�h5�first_tutorial�����}�(hh,hj  ubahj  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �first_tutorial�j  �h]�h]�h!]��	refdomain�j'  uubh5�.�����}�(hNhj$  hNhhhj�  ubehj�  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hKIhhhhhh`h�jThe following sections describe in some detail the philosophy and implementation
of the ``anx`` simulator:�h]�(h5�XThe following sections describe in some detail the philosophy and implementation
of the �����}�(hNh�XThe following sections describe in some detail the philosophy and implementation
of the �hNhhhj?  ubj}  )��}�(hj|  h�``anx``�h]�h5�anx�����}�(hh,hjH  ubahj?  h}�(h]�h]�h]�h!]�h]�uubh5� simulator:�����}�(hNh� simulator:�hNhhhj?  ubehj�  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hKLhhhhhh�hh,h]�(h�)��}�(hNhhhhhh�h�:ref:`simulation_model`�h]�ha)��}�(hKLhhhh`hjf  h]�j�  )��}�(hKLhhhj�  hjf  h]�j   )��}�(hjO  hjf  h]�h5�simulation_model�����}�(hh,hjn  ubahjk  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahjh  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �simulation_model�j  �h]�h]�h!]��	refdomain�jz  uubahjd  h}�(h]�h]�h]�h!]�h]�uubahja  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`packet_generation`�h]�ha)��}�(hKMhhhh`hj�  h]�j�  )��}�(hKMhhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�packet_generation�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �packet_generation�j  �h]�h]�h!]��	refdomain�j�  uubahj�  h}�(h]�h]�h]�h!]�h]�uubahja  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`fwd_queue_implementation`�h]�ha)��}�(hKNhhhh`hj�  h]�j�  )��}�(hKNhhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�fwd_queue_implementation�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �fwd_queue_implementation�j  �h]�h]�h!]��	refdomain�j�  uubahj�  h}�(h]�h]�h]�h!]�h]�uubahja  h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hNhhhhhh�h�:ref:`pkt_travel_time`

�h]�ha)��}�(hKOhhhh`h�:ref:`pkt_travel_time`�h]�j�  )��}�(hKOhhhj�  hj�  h]�j   )��}�(hjO  hj�  h]�h5�pkt_travel_time�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�(j  �std��std-ref�eh]�uubahj�  h}�(j  j  �refexplicit��h]�h]��reftype��ref�j  �pkt_travel_time�j  �h]�h]�h!]��	refdomain�j  uubahj�  h}�(h]�h]�h]�h!]�h]�uubahja  h}�(h]�h]�h]�h!]�h]�uubehj�  h}�(h]�h]�j�  j�  h]�h]�h!]�uubh �comment���)��}�(hKThhhhhj+  h�	Raw links�h]�h5�	Raw links�����}�(hh,hj-  ubahj�  h}�(h]�h]��	xml:space�ji  h]�h]�h!]�uubh �substitution_definition���)��}�(hKUhhhhhj<  h�_.. |Networkx| raw:: html

   <a href="https://networkx.github.io" target="_blank">NetworkX</a>
�h]�j\  )��}�(hKUhhhj[  hj_  h]�h5�A<a href="https://networkx.github.io" target="_blank">NetworkX</a>�����}�(hh,hjB  ubahj>  h}�(h]�h]�j8  ji  �format�jk  h]�h]�h!]�uubahj�  h}�(h]��Networkx�ah]�h]�h!]�h]�uubj=  )��}�(hKYhhhhhj<  h�f.. |SimPy| raw:: html

   <a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>

�h]�j\  )��}�(hKYhhhj[  hjv  h]�h5�J<a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>�����}�(hh,hj[  ubahjW  h}�(h]�h]�j8  ji  �format�j�  h]�h]�h!]�uubahj�  h}�(h]��SimPy�ah]�h]�h!]�h]�uubh �note���)��}�(hNhhh�source/feedback_box.rst�hjp  h�KFound erros or have comments about this page? E-mail anxsimulator@gmail.com�h]�ha)��}�(hKhjt  hh`hju  h]�(h5�5Found erros or have comments about this page? E-mail �����}�(h�5Found erros or have comments about this page? E-mail �hjw  ubh�)��}�(hh�h�anxsimulator@gmail.com�h]�h5�anxsimulator@gmail.com�����}�(hh,hj  ubahjw  h}�(h]�h]��refuri��mailto:anxsimulator@gmail.com�h]�h]�h!]�uubehjr  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��how is this guide organised�ah]�j#  ah]�h!]�h]�uubehhh}�(h]�(�summary�h+eh]�(�summary�heh]�h!]�h]�uubehh�settings��docutils.frontend��Values���)��}�(�strip_comments�N�output_encoding��utf-8��file_insertion_enabled���smart_quotes���dump_pseudo_xml�N�	datestamp�N�cloak_email_addresses���record_dependencies�N�exit_status_level�K�dump_internals�N�toc_backlinks��entry��	generator�N�warning_stream�N�report_level�K�embed_stylesheet���rfc_base_url��https://tools.ietf.org/html/��error_encoding��UTF-8��_destination�N�sectnum_xform�K�source_link�N�expose_internals�N�_config_files�]�h.N�footnote_backlinks�K�output_encoding_error_handler��strict��pep_references�N�strict_visitor�N�sectsubtitle_xform���	traceback���input_encoding��	utf-8-sig��smartquotes_locales�N�doctitle_xform���language_code��en��strip_classes�N�
source_url�N�debug�N�syntax_highlight��long��
halt_level�K�rfc_references�N�gettext_compact���_disable_config�N�raw_enabled�K�character_level_inline_markup���trim_footnote_reference_space���env�N�	tab_width�K�input_encoding_error_handler�j�  �pep_file_url_template��pep-%04d��pep_base_url�� https://www.python.org/dev/peps/��dump_settings�N�strip_elements_with_classes�N�auto_id_prefix��id��_source�h�error_encoding_error_handler��backslashreplace��dump_transforms�N�docinfo_xform�K�	id_prefix�h,�config�Nub�indirect_targets�]��reporter�N�transform_messages�]�(h �system_message���)��}�(hj�  hh,h]�ha)��}�(hh`hh,h]�h5�2Hyperlink target "introduction" is not referenced.�����}�(hh,hj   ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type��INFO��source�hh]�h]�h!]��line�K�level�Kuubj�  )��}�(hj�  hh,h]�ha)��}�(hh`hh,h]�h5�-Hyperlink target "index-0" is not referenced.�����}�(hh,hj  ubahj  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type�j  �source�hh]�h]�h!]��line�K�level�Kuube�refnames�}��substitution_defs�}�(jk  jW  jR  j>  uh}�(h]�h]��source�hh]�h]�h!]�u�nameids�}�(j�  j�  j�  j#  h+hj�  j   j>  j@  u�citation_refs�}��autofootnotes�]��
decoration�N�	footnotes�]��id_start�K�transformer�Nh}�(j  h�j�  h%j.  ja  h�h�hh%j%  j  j   j4  h�ja  j@  h�j#  j�  u�symbol_footnotes�]��symbol_footnote_start�K hh,�autofootnote_start�K�refids�}�(h�]�jX  ah]�hau�substitution_names�}�(�networkx�jR  �simpy�jk  u�current_line�N�	citations�]��	nametypes�}�(j�  Nj�  Nh+�j�  Nj>  Nu�autofootnote_refs�]��parse_messages�]�j�  )��}�(hKhhhhhj�  hh,h]�ha)��}�(hh`h�/Duplicate implicit target name: "introduction".�h]�h5�3Duplicate implicit target name: “introduction”.�����}�(hh,hja  ubahj^  h}�(h]�h]�h]�h!]�h]�uubahja  h}�(h]�h]�j.  a�type�j  �source�hh]�h]�h!]��line�K�level�Kuubahh�current_source�N�footnote_refs�}�ub.