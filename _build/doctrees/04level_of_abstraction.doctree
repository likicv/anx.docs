���1      �docutils.nodes��document���)��}�(�symbol_footnote_refs�]��children�]�(h �target���)��}�(�line�Khh�source��@/home/likicv/anx-docs/anx-docs/source/04level_of_abstraction.rst��tagname�h	�	rawsource��.. _level_of_abstraction:�h]��parent�h�
attributes�}�(�refid��level-of-abstraction��ids�]��backrefs�]��names�]��dupnames�]��classes�]�uubh �section���)��}�(�expect_referenced_by_id�}�hhshKhh�expect_referenced_by_name�}��level_of_abstraction�hshhhh#h� �h]�(h �title���)��}�(hKhhhhhh.h�Level of abstraction�h]�h �Text����Level of abstraction�����}�(hNhh2hNhhhh0ubahh%h}�(h]�h]�h]�h!]�h]�uubh �
field_list���)��}�(hK	hhhhhh@hh,h]�(h �field���)��}�(hK	hhhhhhEhh,h]�(h �
field_name���)��}�(hK hhhhJh�
Last Built�h]�h5�
Last Built�����}�(hhNhhLubahhGh}�(h]�h]�h]�h!]�h]�uubh �
field_body���)��}�(hhZh�|today|�h]�h �	paragraph���)��}�(hK	hhhh`hh^h]�h5�Aug 23, 2017�����}�(h�Aug 23, 2017�hhbubahh\h}�(h]�h]�h]�h!]�h]�uubahhGh}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubhF)��}�(hK
hhhhhhEhh,h]�(hK)��}�(hK hhhhJh�Page Status�h]�h5�Page Status�����}�(hh�hhubahh|h}�(h]�h]�h]�h!]�h]�uubh[)��}�(hhZh�Incomplete
�h]�ha)��}�(hK
hhhh`h�
Incomplete�h]�h5�
Incomplete�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubahh|h}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]�h]�h]�h!]�h]�uubh �topic���)��}�(hKhhhhhh�hh,h]�(h/)��}�(hh.h�Contents�h]�h5�Contents�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubh �bullet_list���)��}�(hNhhhNhh�hh,h]�(h �	list_item���)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h �	reference���)��}�(hh�hh,h]�h5�Introduction�����}�(h�Introduction�hh�ubahh�h}�(�refid��introduction�h]��id2�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h�)��}�(hh�hh,h]�h5�)Level of abstraction in network modelling�����}�(h�)Level of abstraction in network modelling�hh�ubahh�h}�(�refid��)level-of-abstraction-in-network-modelling�h]��id3�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubehh�h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��contents�ah]��contents�ah]�h!]�(�contents��local�eh]�uub�sphinx.addnodes��index���)��}�(hKhhhhhj$  hh,h]�hh%h}�(h]�h]��inline��h]�h]�h!]��entries�]�(�single��level of abstraction��index-0�h,Nt�auubh
)��}�(hKhhhhhh	hh,h]�hh%h}�(hj4  h]�h]�h]�h]�h!]�uubh$)��}�(h'}�j4  j6  shKhhh)}�hhhh#hh,h]�(h/)��}�(hKhhhhhh.hh�h]�h5�Introduction�����}�(hNhh�hNhhhjD  ubahj?  h}�(hh�h]�h]�h]�h]�h!]�uubha)��}�(hKhhhhhh`hXS  Our definition of a model (see :ref:`models`) implies that
a model is always a simplified representation of something
that occurs in real world. For this reason, it is usually
said that the model is an *abstraction*. How we achieve
this abstraction, and where do we stop in the description
of the real world to produce the model, is the question
of the *level of abstraction*. What is the correct level
of abstraction depends on many things (including the
modelling objectives), and is probably one of the most
difficult things in modelling. Let's consider how this
applies to network modelling.�h]�(h5�Our definition of a model (see �����}�(hNh�Our definition of a model (see �hNhhhjQ  ubj#  �pending_xref���)��}�(hKhhhjZ  h�:ref:`models`�h]�h j,  ��)��}�(hj,  hj^  h]�h5�models�����}�(hh,hja  ubahj\  h}�(h]�h]�h]�h!]�(�xref��std��std-ref�eh]�uubahjQ  h}�(�refdoc��04level_of_abstraction��refexplicit��h]�h]��reftype��ref��	reftarget��models��refwarn��h]�h]�h!]��	refdomain�jn  uubh5��) implies that
a model is always a simplified representation of something
that occurs in real world. For this reason, it is usually
said that the model is an �����}�(hNh��) implies that
a model is always a simplified representation of something
that occurs in real world. For this reason, it is usually
said that the model is an �hNhhhjQ  ubh �emphasis���)��}�(hj�  h�*abstraction*�h]�h5�abstraction�����}�(hh,hj�  ubahjQ  h}�(h]�h]�h]�h!]�h]�uubh5��. How we achieve
this abstraction, and where do we stop in the description
of the real world to produce the model, is the question
of the �����}�(hNh��. How we achieve
this abstraction, and where do we stop in the description
of the real world to produce the model, is the question
of the �hNhhhjQ  ubj�  )��}�(hj�  h�*level of abstraction*�h]�h5�level of abstraction�����}�(hh,hj�  ubahjQ  h}�(h]�h]�h]�h!]�h]�uubh5��. What is the correct level
of abstraction depends on many things (including the
modelling objectives), and is probably one of the most
difficult things in modelling. Let’s consider how this
applies to network modelling.�����}�(hNh��. What is the correct level
of abstraction depends on many things (including the
modelling objectives), and is probably one of the most
difficult things in modelling. Let's consider how this
applies to network modelling.�hNhhhjQ  ubehj?  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��introduction�ah]�(h�j4  eh]�h!]�h]�uubh$)��}�(hK$hhhhhh#hh,h]�(h/)��}�(hK$hhhhhh.hh�h]�h5�)Level of abstraction in network modelling�����}�(hNhh�hNhhhj�  ubahj�  h}�(hj  h]�h]�h]�h]�h!]�uubha)��}�(hK&hhhhhh`hX  Network model is a simplified description of some real
network of interest. Choosing the level of abstraction
involves making a decision on where do we stop in the
description of the network topology, components, protocols,
modes of operation, and so on. Consider some real-life
network that we are interested in. A description of
such network can vary a great deal in the scope and
detail. One could argue that as a minimum, a network
description would need to identify the network topology:
nodes and the links that connect the nodes.�h]�h5X  Network model is a simplified description of some real
network of interest. Choosing the level of abstraction
involves making a decision on where do we stop in the
description of the network topology, components, protocols,
modes of operation, and so on. Consider some real-life
network that we are interested in. A description of
such network can vary a great deal in the scope and
detail. One could argue that as a minimum, a network
description would need to identify the network topology:
nodes and the links that connect the nodes.�����}�(hNhj�  hNhhhj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hK1hhhhhh`hX�  In order to turn a network model into a dynamic model
capable of simulating some dynamic aspects of the network
(for example, exchange of network packets), one needs
to add more detail to the network description. To hint
at what kind of detail that might be, consider the
following questions. Are all nodes identical in their
dynamic properties, or are there different types of nodes
(for example, are there host nodes and router nodes)?
Is this actually important for the question one is trying
to address, or perhaps can be safely neglected? Are all
the links between nodes identical in their properties,
or there are different types of links (slow links, fast
links, wired links, wireless links)? At what level of
detail network protocols will be described?�h]�h5X�  In order to turn a network model into a dynamic model
capable of simulating some dynamic aspects of the network
(for example, exchange of network packets), one needs
to add more detail to the network description. To hint
at what kind of detail that might be, consider the
following questions. Are all nodes identical in their
dynamic properties, or are there different types of nodes
(for example, are there host nodes and router nodes)?
Is this actually important for the question one is trying
to address, or perhaps can be safely neglected? Are all
the links between nodes identical in their properties,
or there are different types of links (slow links, fast
links, wired links, wireless links)? At what level of
detail network protocols will be described?�����}�(hNhj�  hNhhhj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hK@hhhhhh`hXg  Consider a slightly constructed, trivial example. Let's
assume that our network of interest is some voice-over-IP
communication network. This means that voice audio
signals are converted to network packets and exchanged
over the transmission Control Protocol/Internet Protocol
(TCP/IP) network. At what level of detail are we going
to describe this? We can model exchange of individual
packets, but ignore the true structure of TCP/IP packets
and the details of the TCP/IP protocol. Alternatively,
one may choose to model some structure of TCP/IP packets,
and some aspects of the TCP/IP protocol, but ignore others.�h]�h5Xi  Consider a slightly constructed, trivial example. Let’s
assume that our network of interest is some voice-over-IP
communication network. This means that voice audio
signals are converted to network packets and exchanged
over the transmission Control Protocol/Internet Protocol
(TCP/IP) network. At what level of detail are we going
to describe this? We can model exchange of individual
packets, but ignore the true structure of TCP/IP packets
and the details of the TCP/IP protocol. Alternatively,
one may choose to model some structure of TCP/IP packets,
and some aspects of the TCP/IP protocol, but ignore others.�����}�(hNhj�  hNhhhj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh �note���)��}�(hNhhh�source/feedback_box.rst�hj�  h�KFound erros or have comments about this page? E-mail anxsimulator@gmail.com�h]�ha)��}�(hKhj�  hh`hj�  h]�(h5�5Found erros or have comments about this page? E-mail �����}�(h�5Found erros or have comments about this page? E-mail �hj�  ubh�)��}�(hh�h�anxsimulator@gmail.com�h]�h5�anxsimulator@gmail.com�����}�(hh,hj  ubahj�  h}�(h]�h]��refuri��mailto:anxsimulator@gmail.com�h]�h]�h!]�uubehj�  h}�(h]�h]�h]�h!]�h]�uubahj�  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��)level of abstraction in network modelling�ah]�j   ah]�h!]�h]�uubehhh}�(h]�(�level of abstraction�h+eh]�(h�id1�eh]�h!]�h]�uubehh�settings��docutils.frontend��Values���)��}�(�strip_comments�N�output_encoding��utf-8��file_insertion_enabled���smart_quotes���dump_pseudo_xml�N�	datestamp�N�cloak_email_addresses���record_dependencies�N�exit_status_level�K�dump_internals�N�toc_backlinks��entry��	generator�N�warning_stream�N�report_level�K�embed_stylesheet���rfc_base_url��https://tools.ietf.org/html/��error_encoding��UTF-8��_destination�N�sectnum_xform�K�source_link�N�expose_internals�N�_config_files�]�h.N�footnote_backlinks�K�output_encoding_error_handler��strict��pep_references�N�strict_visitor�N�sectsubtitle_xform���	traceback���input_encoding��	utf-8-sig��smartquotes_locales�N�doctitle_xform���language_code��en��strip_classes�N�
source_url�N�debug�N�syntax_highlight��long��
halt_level�K�rfc_references�N�gettext_compact���_disable_config�N�raw_enabled�K�character_level_inline_markup���trim_footnote_reference_space���env�N�	tab_width�K�input_encoding_error_handler�jQ  �pep_file_url_template��pep-%04d��pep_base_url�� https://www.python.org/dev/peps/��dump_settings�N�strip_elements_with_classes�N�auto_id_prefix��id��_source�h�error_encoding_error_handler��backslashreplace��dump_transforms�N�docinfo_xform�K�	id_prefix�h,�config�Nub�indirect_targets�]��reporter�N�transform_messages�]�(h �system_message���)��}�(hj  hh,h]�ha)��}�(hh`hh,h]�h5�:Hyperlink target "level-of-abstraction" is not referenced.�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type��INFO��source�hh]�h]�h!]��line�K�level�Kuubj�  )��}�(hj  hh,h]�ha)��}�(hh`hh,h]�h5�-Hyperlink target "index-0" is not referenced.�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type�j�  �source�hh]�h]�h!]��line�K�level�Kuube�refnames�}��substitution_defs�}�h}�(h]�h]��source�hh]�h]�h!]�u�nameids�}�(j(  j*  h+hj  j  j!  j   j�  h�u�citation_refs�}��autofootnotes�]��
decoration�N�	footnotes�]��id_start�K�transformer�Nh}�(j  h�j   j�  j*  h%h�h�h�j?  hh%j4  j?  j  h�u�symbol_footnotes�]��symbol_footnote_start�K hh,�autofootnote_start�K�refids�}�(j4  ]�j6  ah]�hau�substitution_names�}��current_line�N�	citations�]��	nametypes�}�(j(  Nh+�j  Nj!  Nj�  Nu�autofootnote_refs�]��parse_messages�]�hh�current_source�N�footnote_refs�}�ub.