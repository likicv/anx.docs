���a      �docutils.nodes��document���)��}�(�symbol_footnote_refs�]��children�]�(h �target���)��}�(�line�Khh�source��F/home/likicv/anx-docs/anx-docs/source/06discrete_event_simulations.rst��tagname�h	�	rawsource��.. _discrete_event_simulations:�h]��parent�h�
attributes�}�(�refid��discrete-event-simulations��ids�]��backrefs�]��names�]��dupnames�]��classes�]�uubh �section���)��}�(�expect_referenced_by_id�}�hhshKhh�expect_referenced_by_name�}��discrete_event_simulations�hshhhh#h� �h]�(h �title���)��}�(hKhhhhhh.h�Discrete event simulations�h]�h �Text����Discrete event simulations�����}�(hNhh2hNhhhh0ubahh%h}�(h]�h]�h]�h!]�h]�uubh �
field_list���)��}�(hK	hhhhhh@hh,h]�(h �field���)��}�(hK	hhhhhhEhh,h]�(h �
field_name���)��}�(hK hhhhJh�
Last Built�h]�h5�
Last Built�����}�(hhNhhLubahhGh}�(h]�h]�h]�h!]�h]�uubh �
field_body���)��}�(hhZh�|today|�h]�h �	paragraph���)��}�(hK	hhhh`hh^h]�h5�Aug 23, 2017�����}�(h�Aug 23, 2017�hhbubahh\h}�(h]�h]�h]�h!]�h]�uubahhGh}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubhF)��}�(hK
hhhhhhEhh,h]�(hK)��}�(hK hhhhJh�Page Status�h]�h5�Page Status�����}�(hh�hhubahh|h}�(h]�h]�h]�h!]�h]�uubh[)��}�(hhZh�Incomplete
�h]�ha)��}�(hK
hhhh`h�
Incomplete�h]�h5�
Incomplete�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubahh|h}�(h]�h]�h]�h!]�h]�uubehhBh}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]�h]�h]�h!]�h]�uubh �topic���)��}�(hKhhhhhh�hh,h]�(h/)��}�(hh.h�Contents�h]�h5�Contents�����}�(hh�hh�ubahh�h}�(h]�h]�h]�h!]�h]�uubh �bullet_list���)��}�(hNhhhNhh�hh,h]�(h �	list_item���)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h �	reference���)��}�(hh�hh,h]�h5�Introduction�����}�(h�Introduction�hh�ubahh�h}�(�refid��introduction�h]��id5�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h�)��}�(hh�hh,h]�h5�%History of discrete-event simulations�����}�(h�%History of discrete-event simulations�hh�ubahh�h}�(�refid��%history-of-discrete-event-simulations�h]��id6�ah]�h]�h]�h!]�uubahh�h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubh�)��}�(hh�hh,h]�ha)��}�(hh`hh,h]�h�)��}�(hh�hh,h]�h5�&Discrete-event simulations of networks�����}�(h�&Discrete-event simulations of networks�hj  ubahj  h}�(�refid��&discrete-event-simulations-of-networks�h]��id7�ah]�h]�h]�h!]�uubahj  h}�(h]�h]�h]�h!]�h]�uubahh�h}�(h]�h]�h]�h!]�h]�uubehh�h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��contents�ah]��contents�ah]�h!]�(�contents��local�eh]�uub�sphinx.addnodes��index���)��}�(hKhhhhhjG  hh,h]�hh%h}�(h]�h]��inline��h]�h]�h!]��entries�]�((�single��dynamical systems��index-0�h,Nt�(jU  �discrete-event simulations�jW  h,Nt�(jU  �continous time simulations�jW  h,Nt�euubh
)��}�(hKhhhhhh	hh,h]�hh%h}�(hjW  h]�h]�h]�h]�h!]�uubh$)��}�(h'}�jW  j]  shKhhh)}�hhhh#hh,h]�(h/)��}�(hKhhhhhh.hh�h]�h5�Introduction�����}�(hNhh�hNhhhjk  ubahjf  h}�(hh�h]�h]�h]�h]�h!]�uubha)��}�(hKhhhhhh`hX6  Dynamical systems are those that change in time, and where time
is a significant variable. There are two fundamental ways how
to simulate dynamical systems: *continuous time simulations* and
*discrete-event simulations*. Continuous time simulations approximate
one or more differential equations, and are suitable for modelling
of systems where the dynamics of interest can be described by
differential equations. There are many dynamical systems where
this is not the case. Examples include customers arriving at the
service counter, factory assembly lines, item-based repair shops,
accumulation (or depletion) of inventory, and so on. The defining
characteristics of such systems is that a significant change in
the system state occurs only at discrete times. Such systems are
best modelled by discrete-event approaches.�h]�(h5��Dynamical systems are those that change in time, and where time
is a significant variable. There are two fundamental ways how
to simulate dynamical systems: �����}�(hNh��Dynamical systems are those that change in time, and where time
is a significant variable. There are two fundamental ways how
to simulate dynamical systems: �hNhhhjx  ubh �emphasis���)��}�(hj�  h�*continuous time simulations*�h]�h5�continuous time simulations�����}�(hh,hj�  ubahjx  h}�(h]�h]�h]�h!]�h]�uubh5� and
�����}�(hNh� and
�hNhhhjx  ubj�  )��}�(hj�  h�*discrete-event simulations*�h]�h5�discrete-event simulations�����}�(hh,hj�  ubahjx  h}�(h]�h]�h]�h!]�h]�uubh5X[  . Continuous time simulations approximate
one or more differential equations, and are suitable for modelling
of systems where the dynamics of interest can be described by
differential equations. There are many dynamical systems where
this is not the case. Examples include customers arriving at the
service counter, factory assembly lines, item-based repair shops,
accumulation (or depletion) of inventory, and so on. The defining
characteristics of such systems is that a significant change in
the system state occurs only at discrete times. Such systems are
best modelled by discrete-event approaches.�����}�(hNhX[  . Continuous time simulations approximate
one or more differential equations, and are suitable for modelling
of systems where the dynamics of interest can be described by
differential equations. There are many dynamical systems where
this is not the case. Examples include customers arriving at the
service counter, factory assembly lines, item-based repair shops,
accumulation (or depletion) of inventory, and so on. The defining
characteristics of such systems is that a significant change in
the system state occurs only at discrete times. Such systems are
best modelled by discrete-event approaches.�hNhhhjx  ubehjf  h}�(h]�h]�h]�h!]�h]�uubjH  )��}�(hK&hhhhhjG  hh,h]�hjf  h}�(h]�h]�jO  �h]�h]�h!]�jS  ]�(jU  �queuing��index-1�h,Nt�auubh
)��}�(hK(hhhhhh	hh,h]�hjf  h}�(hj�  h]�h]�h]�h]�h!]�uubha)��}�(h'}�j�  j�  shK)hhh)}�hhhh`hX�  Discrete-event dynamical systems often exhibit some form of
"queuing". For example, a customer may enter the service
station at 2.07 minutes after the monitoring started. Before
this moment there were zero customers at the service station,
and after this moment there was exactly one customer at the
service station (that is, until more customers arrive). This
is an example of a discrete-event dynamical system, not naturally
described by differential equations. Several new customers
may enter the service station before the first customer was
served, and if there is not extra capacity at the service
counter, these will have to wait for their turn for being
served. It is said that newly arrived customers are *queued*
for the service.�h]�(h5X�  Discrete-event dynamical systems often exhibit some form of
“queuing”. For example, a customer may enter the service
station at 2.07 minutes after the monitoring started. Before
this moment there were zero customers at the service station,
and after this moment there was exactly one customer at the
service station (that is, until more customers arrive). This
is an example of a discrete-event dynamical system, not naturally
described by differential equations. Several new customers
may enter the service station before the first customer was
served, and if there is not extra capacity at the service
counter, these will have to wait for their turn for being
served. It is said that newly arrived customers are �����}�(hNhX�  Discrete-event dynamical systems often exhibit some form of
"queuing". For example, a customer may enter the service
station at 2.07 minutes after the monitoring started. Before
this moment there were zero customers at the service station,
and after this moment there was exactly one customer at the
service station (that is, until more customers arrive). This
is an example of a discrete-event dynamical system, not naturally
described by differential equations. Several new customers
may enter the service station before the first customer was
served, and if there is not extra capacity at the service
counter, these will have to wait for their turn for being
served. It is said that newly arrived customers are �hNhhhj�  ubj�  )��}�(hj�  h�*queued*�h]�h5�queued�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubh5�
for the service.�����}�(hNh�
for the service.�hNhhhj�  ubehjf  h}�(h]�h]�j�  ah]�h!]�h]�uubha)��}�(hK7hhhhhh`hX�  Many complex business systems, for example those found in
transportation, manufacturing and logistics exhibit the above
mentioned characteristics, are naturally discrete-event
dynamical systems. Therefore it is not surprising that
discrete-event simulations are one of the most widely used
modelling technique [#footnote01]_. Discrete-event simulations
are also the method of choice for modelling of packet-based
networks, as elaborated in the next section.�h]�(h5X6  Many complex business systems, for example those found in
transportation, manufacturing and logistics exhibit the above
mentioned characteristics, are naturally discrete-event
dynamical systems. Therefore it is not surprising that
discrete-event simulations are one of the most widely used
modelling technique �����}�(hNhX6  Many complex business systems, for example those found in
transportation, manufacturing and logistics exhibit the above
mentioned characteristics, are naturally discrete-event
dynamical systems. Therefore it is not surprising that
discrete-event simulations are one of the most widely used
modelling technique �hNhhhj�  ubh �footnote_reference���)��}�(�resolved�Khj�  h�[#footnote01]_�h]�h5�1�����}�(hh,hj�  ubahj�  h}�(h�
footnote01�h]��id2�ah]��auto�Kh]�h]�h!]�uubh5��. Discrete-event simulations
are also the method of choice for modelling of packet-based
networks, as elaborated in the next section.�����}�(hNh��. Discrete-event simulations
are also the method of choice for modelling of packet-based
networks, as elaborated in the next section.�hNhhhj�  ubehjf  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��introduction�ah]�(h�jW  eh]�h!]�h]�uubh$)��}�(hKBhhhhhh#hh,h]�(h/)��}�(hKBhhhhhh.hh�h]�h5�%History of discrete-event simulations�����}�(hNhh�hNhhhj  ubahj  h}�(hj  h]�h]�h]�h]�h!]�uubha)��}�(hKDhhhhhh`hX\  The history of discrete-event simulations is tied to the
development of first computers and attempts at computer
simulations [#footnote02]_. Discrete-event simulations
require special programming techniques, and this originally
spurred the development of several new programming
languages. Perhaps the most famous are Simula I and Simula 67,
developed by Ole-Johan Dahl and Kristen Nygaard at the
Norwegian Computing Center in Oslo in the 1960s [#footnote03]_.
While the focus of Simula project was to develop a programming
language specialised for discrete-event simulations,
this project also produced one of the best known programming
paradigms: object-oriented programming. Simula is considered
the first object-oriented language, and is credited with a
number of innovative features that nowadays have spread widely
to general programming languages, including objects, classes,
inheritance, and co-routines. It is interesting that both
Bjarne Stroustrup (creator of C++) and James Gosling
(creator of Java) were strongly influenced by Simula.
Today nobody writes a programming language specifically for
discrete-event simulations, rather modern general purpose
programming languages are quite suitable for this task.
Not surprisingly, languages that are strongly object-oriented
such are C++ and Java are particularly suitable for this
purpose. And of course, Python.�h]�(h5�}The history of discrete-event simulations is tied to the
development of first computers and attempts at computer
simulations �����}�(hNh�}The history of discrete-event simulations is tied to the
development of first computers and attempts at computer
simulations �hNhhhj(  ubj�  )��}�(j�  Khj�  h�[#footnote02]_�h]�h5�2�����}�(hh,hj1  ubahj(  h}�(h�
footnote02�h]��id3�ah]�j  Kh]�h]�h!]�uubh5X2  . Discrete-event simulations
require special programming techniques, and this originally
spurred the development of several new programming
languages. Perhaps the most famous are Simula I and Simula 67,
developed by Ole-Johan Dahl and Kristen Nygaard at the
Norwegian Computing Center in Oslo in the 1960s �����}�(hNhX2  . Discrete-event simulations
require special programming techniques, and this originally
spurred the development of several new programming
languages. Perhaps the most famous are Simula I and Simula 67,
developed by Ole-Johan Dahl and Kristen Nygaard at the
Norwegian Computing Center in Oslo in the 1960s �hNhhhj(  ubj�  )��}�(j�  Khj�  h�[#footnote03]_�h]�h5�3�����}�(hh,hjF  ubahj(  h}�(h�
footnote03�h]��id4�ah]�j  Kh]�h]�h!]�uubh5X�  .
While the focus of Simula project was to develop a programming
language specialised for discrete-event simulations,
this project also produced one of the best known programming
paradigms: object-oriented programming. Simula is considered
the first object-oriented language, and is credited with a
number of innovative features that nowadays have spread widely
to general programming languages, including objects, classes,
inheritance, and co-routines. It is interesting that both
Bjarne Stroustrup (creator of C++) and James Gosling
(creator of Java) were strongly influenced by Simula.
Today nobody writes a programming language specifically for
discrete-event simulations, rather modern general purpose
programming languages are quite suitable for this task.
Not surprisingly, languages that are strongly object-oriented
such are C++ and Java are particularly suitable for this
purpose. And of course, Python.�����}�(hNhX�  .
While the focus of Simula project was to develop a programming
language specialised for discrete-event simulations,
this project also produced one of the best known programming
paradigms: object-oriented programming. Simula is considered
the first object-oriented language, and is credited with a
number of innovative features that nowadays have spread widely
to general programming languages, including objects, classes,
inheritance, and co-routines. It is interesting that both
Bjarne Stroustrup (creator of C++) and James Gosling
(creator of Java) were strongly influenced by Simula.
Today nobody writes a programming language specifically for
discrete-event simulations, rather modern general purpose
programming languages are quite suitable for this task.
Not surprisingly, languages that are strongly object-oriented
such are C++ and Java are particularly suitable for this
purpose. And of course, Python.�hNhhhj(  ubehj  h}�(h]�h]�h]�h!]�h]�uubehh%h}�(h]��%history of discrete-event simulations�ah]�j   ah]�h!]�h]�uubh$)��}�(hK_hhhhhh#hh,h]�(h/)��}�(hK_hhhhhh.hj   h]�h5�&Discrete-event simulations of networks�����}�(hNhj   hNhhhjk  ubahjh  h}�(hj%  h]�h]�h]�h]�h!]�uubha)��}�(hKahhhhhh`hX)  The nature packet-based networks points immediately to their
discrete-event behaviour: nodes generate packets at discrete
(typically stochastic) times and send them to other nodes.
If the destination node is directly connected to the node
that generated the packet, the packet it passed directly.
If the destination node is not directly connected to the source
node, different things may happen depending on the function
of the network. For example, the packet may be discarded,
or passed to the next node along the shortest path to the
destination. A node may be idle, but at any time it may
receive a packet destined to itself. Typically, a node need
some time to process a just received packet: at least it
needs to "read" the destination address from the incoming
packet, in order to determine the packet destination. This
requires a delay (however small) and a node may receive
one or more additional packets during this time. The
additional packet(s) may be dropped, or more commonly,
placed in the queue to wait for their turn at processing.
In the latter case, we have simply the queuing phenomenon,
in other words the behaviour similar to the customers
queuing at the service counter. These characteristics
explain why discrete-event simulations are used most
widely for the simulations of packet-based networks.�h]�h5X-  The nature packet-based networks points immediately to their
discrete-event behaviour: nodes generate packets at discrete
(typically stochastic) times and send them to other nodes.
If the destination node is directly connected to the node
that generated the packet, the packet it passed directly.
If the destination node is not directly connected to the source
node, different things may happen depending on the function
of the network. For example, the packet may be discarded,
or passed to the next node along the shortest path to the
destination. A node may be idle, but at any time it may
receive a packet destined to itself. Typically, a node need
some time to process a just received packet: at least it
needs to “read” the destination address from the incoming
packet, in order to determine the packet destination. This
requires a delay (however small) and a node may receive
one or more additional packets during this time. The
additional packet(s) may be dropped, or more commonly,
placed in the queue to wait for their turn at processing.
In the latter case, we have simply the queuing phenomenon,
in other words the behaviour similar to the customers
queuing at the service counter. These characteristics
explain why discrete-event simulations are used most
widely for the simulations of packet-based networks.�����}�(hNhjz  hNhhhjx  ubahjh  h}�(h]�h]�h]�h!]�h]�uubh �note���)��}�(hNhhh�source/feedback_box.rst�hj�  h�KFound erros or have comments about this page? E-mail anxsimulator@gmail.com�h]�ha)��}�(hKhj�  hh`hj�  h]�(h5�5Found erros or have comments about this page? E-mail �����}�(h�5Found erros or have comments about this page? E-mail �hj�  ubh�)��}�(hh�h�anxsimulator@gmail.com�h]�h5�anxsimulator@gmail.com�����}�(hh,hj�  ubahj�  h}�(h]�h]��refuri��mailto:anxsimulator@gmail.com�h]�h]�h!]�uubehj�  h}�(h]�h]�h]�h!]�h]�uubahjh  h}�(h]�h]�h]�h!]�h]�uubh �rubric���)��}�(hK~hhhhhj�  h�	Footnotes�h]�h5�	Footnotes�����}�(hNhj�  hNhhhj�  ubahjh  h}�(h]�h]�h]�h!]�h]�uubh �footnote���)��}�(hKhhhhhj�  h��See: Robinson S, "Discrete-event simulation: from the pioneers to the present, what next?", Journal of the Operational Research Society, Volume 56, Issue 6, pp 619–629 (2005)
�h]�(h �label���)��}�(hNhhhNhj�  hh,h]�h5�1�����}�(hNhh,hNhhhj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hKhhhh`h��See: Robinson S, "Discrete-event simulation: from the pioneers to the present, what next?", Journal of the Operational Research Society, Volume 56, Issue 6, pp 619–629 (2005)�h]�h5��See: Robinson S, “Discrete-event simulation: from the pioneers to the present, what next?”, Journal of the Operational Research Society, Volume 56, Issue 6, pp 619–629 (2005)�����}�(hj�  hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubehjh  h}�(h]�j�  ah]�j   aj  Kh]��
footnote01�ah]�h!]�uubj�  )��}�(hK�hhhhhj�  hXe  See for example: Nance RE, "A History of Discrete Event Simulation Programming Languages", Technical Report TR-93-21, Computer Science, Virginia Polytechnic Institute and State University; Hollocks BW, "Forty Years of Discrete-Event Simulation: A Personal Reflection", The Journal of the Operational Research Society Volume 57, No. 12, pp. 1383-1399 (2006)
�h]�(j�  )��}�(hNhhhNhj�  hh,h]�h5�2�����}�(hNhh,hNhhhj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hK�hhhh`hXd  See for example: Nance RE, "A History of Discrete Event Simulation Programming Languages", Technical Report TR-93-21, Computer Science, Virginia Polytechnic Institute and State University; Hollocks BW, "Forty Years of Discrete-Event Simulation: A Personal Reflection", The Journal of the Operational Research Society Volume 57, No. 12, pp. 1383-1399 (2006)�h]�h5Xl  See for example: Nance RE, “A History of Discrete Event Simulation Programming Languages”, Technical Report TR-93-21, Computer Science, Virginia Polytechnic Institute and State University; Hollocks BW, “Forty Years of Discrete-Event Simulation: A Personal Reflection”, The Journal of the Operational Research Society Volume 57, No. 12, pp. 1383-1399 (2006)�����}�(hj�  hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubehjh  h}�(h]�j:  ah]�j<  aj  Kh]��
footnote02�ah]�h!]�uubj�  )��}�(hK�hhhhhj�  h��For many interesting details on Simula, see: Dahl, O.-J. "The Birth of Object Orientation: the Simula Languages", Object-Orientation to Formal Methods, Volume 2635 of the series Lecture Notes in Computer Science pp 15-25.�h]�(j�  )��}�(hNhhhNhj�  hh,h]�h5�3�����}�(hNhh,hNhhhj  ubahj  h}�(h]�h]�h]�h!]�h]�uubha)��}�(hK�hhhh`hj  h]�h5��For many interesting details on Simula, see: Dahl, O.-J. “The Birth of Object Orientation: the Simula Languages”, Object-Orientation to Formal Methods, Volume 2635 of the series Lecture Notes in Computer Science pp 15-25.�����}�(hj  hj"  ubahj  h}�(h]�h]�h]�h!]�h]�uubehjh  h}�(h]�jO  ah]�jQ  aj  Kh]��
footnote03�ah]�h!]�uubehh%h}�(h]��&discrete-event simulations of networks�ah]�j#  ah]�h!]�h]�uubehhh}�(h]�(�discrete event simulations�h+eh]�(h�id1�eh]�h!]�h]�uubehh�settings��docutils.frontend��Values���)��}�(�strip_comments�N�output_encoding��utf-8��file_insertion_enabled���smart_quotes���dump_pseudo_xml�N�	datestamp�N�cloak_email_addresses���record_dependencies�N�exit_status_level�K�dump_internals�N�toc_backlinks��entry��	generator�N�warning_stream�N�report_level�K�embed_stylesheet���rfc_base_url��https://tools.ietf.org/html/��error_encoding��UTF-8��_destination�N�sectnum_xform�K�source_link�N�expose_internals�N�_config_files�]�h.N�footnote_backlinks�K�output_encoding_error_handler��strict��pep_references�N�strict_visitor�N�sectsubtitle_xform���	traceback���input_encoding��	utf-8-sig��smartquotes_locales�N�doctitle_xform���language_code��en��strip_classes�N�
source_url�N�debug�N�syntax_highlight��long��
halt_level�K�rfc_references�N�gettext_compact���_disable_config�N�raw_enabled�K�character_level_inline_markup���trim_footnote_reference_space���env�N�	tab_width�K�input_encoding_error_handler�jh  �pep_file_url_template��pep-%04d��pep_base_url�� https://www.python.org/dev/peps/��dump_settings�N�strip_elements_with_classes�N�auto_id_prefix��id��_source�h�error_encoding_error_handler��backslashreplace��dump_transforms�N�docinfo_xform�K�	id_prefix�h,�config�Nub�indirect_targets�]��reporter�N�transform_messages�]�(h �system_message���)��}�(hj�  hh,h]�ha)��}�(hh`hh,h]�h5�@Hyperlink target "discrete-event-simulations" is not referenced.�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type��INFO��source�hh]�h]�h!]��line�K�level�Kuubj�  )��}�(hj�  hh,h]�ha)��}�(hh`hh,h]�h5�-Hyperlink target "index-0" is not referenced.�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type�j�  �source�hh]�h]�h!]��line�K�level�Kuubj�  )��}�(hj�  hh,h]�ha)��}�(hh`hh,h]�h5�-Hyperlink target "index-1" is not referenced.�����}�(hh,hj�  ubahj�  h}�(h]�h]�h]�h!]�h]�uubah}�(h]�h]��type�j�  �source�hh]�h]�h!]��line�K(�level�Kuube�refnames�}�(�
footnote01�]�j�  a�
footnote02�]�j1  a�
footnote03�]�jF  au�substitution_defs�}�h}�(h]�h]��source�hh]�h]�h!]�u�nameids�}�(jc  j   h+hj  j:  j  h�j8  j#  j�  j�  j>  j@  j?  jA  j3  jO  u�citation_refs�}��autofootnotes�]�(j�  j�  j  e�
decoration�N�	footnotes�]��id_start�K�transformer�Nh}�(j�  j�  j#  jh  j   j  h�jf  j  h�jW  jf  h�h�jA  h%j   j�  jQ  jF  j%  j  j:  j�  hh%j<  j1  j@  h�j�  j�  jO  j  u�symbol_footnotes�]��symbol_footnote_start�K hh,�autofootnote_start�K�refids�}�(j�  ]�j�  aj:  ]�j1  ajO  ]�jF  ah]�haj�  ]�j�  ajW  ]�j]  au�substitution_names�}��current_line�N�	citations�]��	nametypes�}�(jc  Nh+�j  �j  Nj8  Nj�  �j>  Nj?  Nj3  �u�autofootnote_refs�]�(j�  j1  jF  e�parse_messages�]�hh�current_source�N�footnote_refs�}�(j�  ]�j�  aj�  ]�j1  aj�  ]�jF  auub.