.. _network_simulation:


===================================
Introduction to network simulations
===================================

:Page Status: Incomplete
:Last Reviewed: 2017-04-22

.. contents:: Contents
   :local:

.. index::
   single: network simulations


What is a network simulation?
-----------------------------

Historically, network simulations have been developed most extensively
in the area of communication and computer networks. However, in today's
world, network simulations are increasingly being used to study social
systems, as many social phenomena can be modelled as a network
phenomena (for example, the spread of infectious disease, the spread
of some information or a belief through a social network, and so on).
What do we mean by network simulation? We use this term to denote
a computer simulation that approximates the dynamic behaviour of
some network of interest. There are many reasons why one might perform
such a simulation, including exploratory research, the analysis of
specific network protocols, and how the network performs under certain
conditions. Therefore, network simulations are typically a part of
some research project concerned with network function or performance.


Computer simulation versus physical experimentation
---------------------------------------------------

In communication and computer networks research simulations offer
an alternative to setting up the laboratory with real hardware and
performing experiments. While computer simulations and physical
experiments are two different approaches, they overlap in questions
that they can address. Furthermore, often computer simulations can
address questions that are difficult or even impossible to address
by physical experiments. However, one needs always to keep in mind
that computer simulations are not the reality itself; they are
merely a model of some reality. This has two important implications.
Firstly, the results of computer simulations are always an
approximation of reality that is being modelled (even when a very
good approximation). Secondly, the results of computer simulations
always require a validation and verification by physical experiments.

There are some advantages offered by computer simulations relative
to physical experiments. The first was already mentioned above:
the ability to consider scenarios that are difficult or even
impossible to perform in a physical experiment. The second is
the cost: computer simulations are usually significantly less
expensive when compared to equivalent physical experiments.
Thanks to the low price and incredible computing power of modern
computers, one can perform hundreds or thousands of computer
simulations with little cost. For these reasons, computer
simulations are particularly useful in the early or exploratory
stages of research. Computer simulations often can eliminate
vast areas of the problem space and in this way contribute
significantly to the overall problem solving. Simulations can
provide illuminating insights how to design actual experiments,
thus saving the time and effort in the long term.


.. index::
   single: network emulation


Network simulation *versus* network emulation
---------------------------------------------

A confusing matter of terminology is the difference between
*network simulation* and *network emulation*. A simulation
is running a computational model that simulates the reality
entirely within the virtual, computer world. In contrast,
network emulation is a physical experiment that involves
components of a real network. In network emulation real
network elements (hosts, routers, links) may be combined
with simulated elements. For example, synthetic network
traffic may be injected into the network. Even when this
is the case, overall effort would still be considered a
network emulation, rather than simulation.

Since in network emulation there are physical network
components involved, the emulation time corresponds to
real time, measured in seconds, minutes and hours.
In contrast, in network simulation the entire network model
in constructed within a virtual, computer environment.
One consequence of this is that in network simulation
the simulation time does not necessarily correspond to
real time. For example, simulating a few hours of network
operation may require a few milliseconds or a few days
of computer time. This depends on the level of abstraction
or granularity in representation, and the power of the
computer used for the simulation. A simulation that models
network in great detail may require a lot of computing power
to execute. This of course translates into extended
simulation time. If such a simulation is run on a slow
computer, the simulation may require long time to execute.
