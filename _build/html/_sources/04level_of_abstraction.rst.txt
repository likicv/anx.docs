.. _level_of_abstraction:


====================
Level of abstraction
====================


:Last Built: |today|
:Page Status: Incomplete

.. contents:: Contents
   :local:

.. index::
   single: level of abstraction


Introduction
------------

Our definition of a model (see :ref:`models`) implies that
a model is always a simplified representation of something
that occurs in real world. For this reason, it is usually
said that the model is an *abstraction*. How we achieve
this abstraction, and where do we stop in the description
of the real world to produce the model, is the question
of the *level of abstraction*. What is the correct level
of abstraction depends on many things (including the
modelling objectives), and is probably one of the most
difficult things in modelling. Let's consider how this
applies to network modelling.


Level of abstraction in network modelling
-----------------------------------------

Network model is a simplified description of some real
network of interest. Choosing the level of abstraction
involves making a decision on where do we stop in the
description of the network topology, components, protocols,
modes of operation, and so on. Consider some real-life
network that we are interested in. A description of
such network can vary a great deal in the scope and
detail. One could argue that as a minimum, a network
description would need to identify the network topology:
nodes and the links that connect the nodes.

In order to turn a network model into a dynamic model
capable of simulating some dynamic aspects of the network
(for example, exchange of network packets), one needs
to add more detail to the network description. To hint
at what kind of detail that might be, consider the
following questions. Are all nodes identical in their
dynamic properties, or are there different types of nodes 
(for example, are there host nodes and router nodes)?
Is this actually important for the question one is trying
to address, or perhaps can be safely neglected? Are all
the links between nodes identical in their properties,
or there are different types of links (slow links, fast
links, wired links, wireless links)? At what level of
detail network protocols will be described?

Consider a slightly constructed, trivial example. Let's
assume that our network of interest is some voice-over-IP
communication network. This means that voice audio
signals are converted to network packets and exchanged
over the transmission Control Protocol/Internet Protocol
(TCP/IP) network. At what level of detail are we going
to describe this? We can model exchange of individual
packets, but ignore the true structure of TCP/IP packets
and the details of the TCP/IP protocol. Alternatively,
one may choose to model some structure of TCP/IP packets,
and some aspects of the TCP/IP protocol, but ignore others.


.. include:: feedback_box.rst
