.. _pkt_travel_time:


==================
Packet travel time
==================

:Page Status: Incomplete
:Last Reviewed: 2017-05-19

.. contents:: Contents
   :local:

.. index::
   single: packet travel time

.. note::  This is an advanced topic

Armed with the understanding how how is the forwarding queue
implemented (see the section :ref:`fwd_queue_implementation`),
in this section we look closely at the output of the two-node
simulation discussed in the section :ref:`packet_generation`.
Our objective is to understand 
