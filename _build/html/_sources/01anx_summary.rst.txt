.. _introduction:


=======
Summary
=======


:Last Built: |today|
:Page Status: Incomplete

.. contents:: Contents
   :local:

.. index::
   single: summary description


Introduction
------------

Abstract Network Simulator (``anx``) is a generic discrete-event
network simulator for networks that exchange discrete information
packets. ``anx`` allows one to investigate network dynamics in
communication networks and social networks, where they can be
approximated as packet-based information exchange networks.
``anx`` aims for a high level of abstraction, and allows one to
study dynamics of networks of arbitrary size and topology.
``anx`` also provides a simulation framework that can be easily
extended to model specific behaviours.

``anx`` is primarily a research and education tool. It may be of
interest to researchers interested in a specific problem related
to network dynamics, and  to anyone learning about network
simulations. ``anx`` is written in Python, a high-level programming
language widely used in science and network research. For more
details see :ref:`python`.


Implementation
--------------

``anx`` is written in pure Python3, and has two core dependencies
outside Python standard libraries: |NetworkX| and |SimPy|.
NetworkX is a library for creation and manipulation of complex
networks, where these are modeled, viewed and manipulated as
graphs. SimPy is a general library for discrete-event simulations
framework that relies on Python generators (first introduced in
Python 2.5). ``anx`` uses NetworkX to create and manipulate network
representations, and once these are converted into a computational
model and uses SimPy for the simulation of network traffic.


How is this guide organised
---------------------------

This guide is organised into a number of sections. The next section
shows the quick installation instructions (:ref:`installation`).
The following sections are a general discussion on modelling and how
it applies to network simulations:

* :ref:`models`
* :ref:`level_of_abstraction`
* :ref:`network_simulation`
* :ref:`discrete_event_simulations`
* :ref:`python`


These sections are not specific to ``anx`` simulator. If you are interested
to get a quick perspective on how to start working with the ``anx`` simulator,
jump here: :ref:`first_tutorial`.

The following sections describe in some detail the philosophy and implementation
of the ``anx`` simulator:

* :ref:`simulation_model`
* :ref:`packet_generation`
* :ref:`fwd_queue_implementation`
* :ref:`pkt_travel_time`


.. Raw links


.. |Networkx| raw:: html

   <a href="https://networkx.github.io" target="_blank">NetworkX</a>

.. |SimPy| raw:: html

   <a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>


.. include:: feedback_box.rst
