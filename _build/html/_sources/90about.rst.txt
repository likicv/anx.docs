.. _about:


================
About this guide
================


:Last Built: |today|
:Page Status: Incomplete

.. contents:: Contents
   :local:

.. index::
   single: about this guide


This guide was written on |Debian_Linux| running |Xfce_Desktop_Environment|.
The text was written with |Vim| text editor, and processed with |Sphinx|
documentation generator by using |reStructuredText|. Graphics was created
with |Xfig|, |Octave| and |Gnuplot|. 

We are grateful to |Bitbucket| for hosting the guide repository and to
|Read_the_Docs| for publishing the guide. 


.. include:: feedback_box.rst


.. Raw links


.. |Debian_Linux| raw:: html

   <a href="https://www.debian.org" target="_blank">Debian Linux</a>


.. |Xfce_Desktop_Environment| raw:: html

   <a href="https://www.xfce.org" target="_blank">Xfce Desktop Environment</a>


.. |Vim| raw:: html

   <a href="http://www.vim.org" target="_blank">Vim</a>


.. |Sphinx| raw:: html

   <a href="http://www.sphinx-doc.org/" target="_blank">Sphinx</a>


.. |reStructuredText| raw:: html

   <a href="http://docutils.sourceforge.net/rst.html" target="_blank">reStructuredText</a>


.. |Xfig| raw:: html

   <a href="http://mcj.sourceforge.net" target="_blank">Xfig</a>


.. |Octave| raw:: html

   <a href="https://www.gnu.org/software/octave" target="_blank">GNU Octave</a>


.. |Gnuplot| raw:: html

   <a href="http://www.gnuplot.info" target="_blank">Gnuplot</a>


.. |Bitbucket| raw:: html

   <a href="https://bitbucket.org" target="_blank">Bitbucket</a>


.. |Read_the_Docs| raw:: html

   <a href="https://readthedocs.org" target="_blank">Read the Docs</a>
