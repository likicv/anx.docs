.. _network_visualisation:


=====================
Network visualisation
=====================


:Last Built: |today|
:Page Status: Complete

.. contents:: Contents
   :local:

.. index::
   single: network visualisation


Introduction
------------

In this section we visualise random 128-node Barabasi-Albert network such
as one used to create the simulation in the section :ref:`real_network`.
For this we use |NetworkX|. Note that the plotting from |NetworkX| requires
that the Python package |matplotlib| is installed. To keep things simple
we first generate a random network topology (in the same way as used in
section :ref:`real_network`), and plot:


.. code-block:: python

    import networkx as nx
    import matplotlib.pyplot as mpl

    G = nx.barabasi_albert_graph(128, 7)

    layout = nx.spring_layout(G)
    nx.draw(G, layout)
    mpl.show()


Running this script generates the following figure:


.. figure:: images/42network_visualisation/barabasi-albert-1.png
   :scale: 50 %

   A random 128-node Barabasi-Albert network generated with spring layout


Running the script repeatedly will generate a different network and a different
figure each time. Here we used the spring layout (``nx.spring_layout(G)``).
|NetworkX| offers other layouts as well. For example, using the random layout
(``nx.random_layout(G)``) positions the nodes uniformly at random in the square:


.. code-block:: python

    import networkx as nx
    import matplotlib.pyplot as mpl

    G = nx.barabasi_albert_graph(128, 7)

    layout = nx.random_layout(G)
    nx.draw(G, layout)
    mpl.show()


Running this script generates the following figure:


.. figure:: images/42network_visualisation/barabasi-albert-2.png
   :scale: 50 %

   A random 128-node Barabasi-Albert network generated with random layout


Visualising network topology from the ``.topo`` file
----------------------------------------------------

In the section :ref:`real_network` we first generated the ``.topo`` file,
then run the simulation. This in fact is only one possible way to generate
the ``.topo`` file, which may come from many different sources. How to 
visualise a network topology from a given ``.topo`` file? This is simply
a matter of creating a |NetworkX| graph object from the ``.topo`` file,
then network topology visualisation can be achieved in the same way as
shown in the previous section.

How can one create a |NetworkX| graph object from the ``.topo`` file?
``anx`` provides the function that loads the network links from the
``.topo`` file (function ``load_links()``, usually used when creating
a network model). The resulting links can be converted into the |NetworkX|
graph object with the helper function ``links2graph()``. Both functions
``load_links()`` and ``links2graph()`` are located in the module
``ModelUtils``. The following example shows how to create a |NetworkX|
graph object from the file "barabasi_albert.topo" located in the
directory :samp:`./network`:


.. code-block:: python

    from anx import ModelUtils

    links = ModelUtils.load_links("barabasi_albert", input_dir = "./network")
    G = ModelUtils.links2graph(links)


Once the the |NetworkX| graph object ``G`` is created, it can be visualised as
shown above.


Visualising network topology with R
-----------------------------------


.. index::
   single: R


|R| is a power statistical package with significant capabilities in network
analysis and visualisation. One can visualise the network topology in |R|
by using the package ``igraph``. Conveniently, ``igraph`` can read the network
topology directly from ``.topo`` file, as follows:


.. code-block:: R

   library('igraph')

   dd <- read.table('barabasi_albert.topo')
   g <- graph.data.frame(dd, directed=FALSE)

   plot(g)


The resulting figure is shown below. 

.. figure:: images/42network_visualisation/barabasi-albert-R-1.png
   :scale: 75 %

   Network visualisation of the ``anx`` topology file created in R with the package 'igraph'.


The |R| package ``igraph`` is quite powerful and allows extensive customisation
of how the network graph is displayed (explaining this is beyond the scope of
this guide). The snippet below shows how to plot the same network with a different
layout:


.. code-block:: R

   library('igraph')

   dd <- read.table('barabasi_albert.topo')
   g <- graph.data.frame(dd, directed=FALSE)

   layout=layout.reingold.tilford(g, circular=T)
   plot(g, layout=layout)


The resulting figure is shown below. 

.. figure:: images/42network_visualisation/barabasi-albert-R-2.png
   :scale: 75 %

   Network visualisation of the ``anx`` topology file created in R with the package 'igraph', by using a Reingold-Tilford layout 


.. Raw links


.. |Networkx| raw:: html

   <a href="https://networkx.github.io" target="_blank">NetworkX</a>

.. |matplotlib| raw:: html

   <a href="https://matplotlib.org" target="_blank">matplotlib</a>

.. |R| raw:: html

   <a href="https://www.r-project.org" target="_blank">R</a>
