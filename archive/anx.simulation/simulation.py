"""simulation.py
"""

# pylint: disable=invalid-name

from anx import ModelUtils
from anx import Utils
from anx import Driver
from anx import Globals

# set model to use and paths
model_name = "three_linear_01"
input_dir = "./input"
output_dir = "./output"

# set simulation time
sim_time = 2

# set verbose level
#   Globals.VERB_HI - all available info
#   Globals.VERB_LO - intermediate info
#   Globals.VERB_NO - minimal info
verbose_level = Globals.VERB_NO

print("\n Working on model '{:s}'\n".format(model_name))

print(" Loading model definitions..")
model_edges = ModelUtils.load_network_topo(model_name, input_dir)
node_types = ModelUtils.load_node_types(model_name, input_dir)
model_param = ModelUtils.load_model_param(model_name, input_dir)

print("\n Verifying model input..")
ModelUtils.verify_model_input(model_edges, node_types, model_param)

print("\n Creating computational model..")
G = ModelUtils.make_model(model_edges, node_types, model_param,
                          model_name)

print("\n Running simulation on model '{:s}'".format(G.graph["model_name"]))

Driver.run_sim(G, sim_time, output_dir, verbose_level)

print("")
Utils.clean_up(Globals.RM_FILES)
