
def a(x, y):
    print(x,y)

def b(other, f, *args, **kwargs):
    f(*args, **kwargs)
    print(other)

b('world', a, 'hello', 'dude')
