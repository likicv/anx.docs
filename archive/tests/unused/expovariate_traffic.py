import random
import simpy

def net_traffic_generator(env):
    while True:
        yield env.timeout(random.expovariate(19))
        print("packet generated @ {:.4f}".format(env.now))

env = simpy.Environment()
env.process(net_traffic_generator(env))
env.run(until=100)
