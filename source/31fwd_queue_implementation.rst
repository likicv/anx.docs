.. _fwd_queue_implementation:

===============================
Forwarding queue implementation
===============================


:Last Built: |today|
:Page Status: Incomplete

.. contents:: Contents
   :local:

.. index::
   single: self.proc_queue
   single: self.forward_process()
   single: fowarding queue

.. note::  This is an advanced topic


In ``anx`` a node is modelled by the class ``anx.Component.Node``, and
forwarding queue is represented by the attribute ``self.proc_queue``.
Node forwarding process is implemented as method called
``self.forward_process()``, and this method actually implements
a SimPy process.

Below is the code for ``self.forward_process()``, which is a method of
class ``anx.Component.Node``:


.. code-block:: python

   def forward_process(self):

       while True:

           # if forwarding queue is not empty
           if len(self.proc_queue) > 0:

               # get the first packet from the queue
               pkt = self.proc_queue.popleft()

               dest_node = pkt[Globals.DEST_NODE]

               if dest_node == self.name:
                   pkt[Globals.DEST_TIME_STAMP] = self.env.now
                   self.received.append([self.env.now, pkt])
                   yield self.env.timeout(0)
               else:
                   yield self.env.timeout(self.fwd_delay)
                   hop_node = self.path_G[(self.name, dest_node)]
                   pkt[Globals.HOP_NODE] = hop_node
                   self.send_to_node(pkt, hop_node)
            
           else: # forwarding queue is empty
               #incur the queue checking delay
               yield self.env.timeout(self.queue_check)


Let's consider more closely what this method does, focusing on the
attribute ``self.proc_queue``.
First, note that the method ``self.forward_process()`` is a SimPy process
method. To understand this please consult |SimPy_link|.

.. |SimPy_link| raw:: html

   <a href="https://simpy.readthedocs.io/en/latest/" target="_blank">SimPy documentation</a>

The outer loop construct in above code snipped is basically the
following:


.. code-block:: python

   def forward_process(self):

       while True:

           if len(self.proc_queue) > 0: # if forwarding queue is not empty

               # do something with 'self.proc_queue'
            
           else: # forwarding queue is empty
               # incur the queue checking delay
               yield self.env.timeout(self.queue_check)


This shows that ``self.forward_process()`` is periodically testing if
there are any packets within the forwarding queue, ``self.proc_queue``.

A bit of digression: because of the ``while True:`` statement,
``self.forward_process()`` is seemingly looping forever and testing
if the forwarding queue is empty. This infinite loop is a notation
used by SimPy processes. The loop in fact will be terminated by the
SimPy environment when the specified simulation time is reached.

Now let's consider what the loop does. If there are no packets stored
in ``self.proc_queue``, then the SimPy timeout event is executed
(``self.env.timeout(self.queue_check)``).
This simply causes a delay of ``self.queue_check`` time units.
Then the execution returns to the top of the loop, that is,
checking again if there are any packets in the queue.


.. index::
   single: node_queue_check


The attribute ``self.queue_check`` of the class ``anx.Component.Node``
is set with the parameter ``node_queue_check`` given in the in the
'.param' input file.
As an example, in the section :ref:`Packet generation <packet_generation>`,
we used the following '.param' file:


.. code-block:: json 
    :emphasize-lines: 5

    {
    "n" : {
          "node_pkt_rate" : [ "poisson", 19 ],
          "node_proc_delay" : 0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }

And ``node_queue_check`` was set to 0.001 for all nodes of the
type 'n' (highlighted). Again, all time references are expressed
in arbitrary time units (TU), including ``node_queue_check``.

Let's consider what is happening within the ``if`` block statement,
executed when one or more packets are detected in the forwarding
queue. The relevant code snipped is extracted below:


.. code-block:: python

    pkt = self.proc_queue.popleft()

    dest_node = pkt[Globals.DEST_NODE]

    if dest_node == self.name:
        pkt[Globals.DEST_TIME_STAMP] = self.env.now
        self.received.append([self.env.now, pkt])
        yield self.env.timeout(0)
    else:
        yield self.env.timeout(self.fwd_delay)
        hop_node = self.path_G[(self.name, dest_node)]
        pkt[Globals.HOP_NODE] = hop_node
        self.send_to_node(pkt, hop_node)


The first command gets the packet from the forwarding
queue, and stores the packet under the variable
``pkt``, and the second command gets the destination
node from the packet.

Once the packet is fetched from the forwarding queue
it is processed in one of the two possible ways (handled
by the ``if-else`` statement). If the destination node is
the same as the node that owns the forwarding queue,
the packet has reached the destination and is placed
in the node sink (the attribute named ``self.received``).

If the packet destination node is different then the
node owning the forwarding queue, four things happen.
We extract the code below:


.. code-block:: python

    yield self.env.timeout(self.fwd_delay)
    hop_node = self.path_G[(self.name, dest_node)]
    pkt[Globals.HOP_NODE] = hop_node
    self.send_to_node(pkt, hop_node)


.. index::
   single: self.fwd_delay


First, the forwarding queue processing delay is incurred.
This is specified by the attribute ``self.fwd_delay``,
expressed in TU.
Second, the next-hop node is retrieved from the precomputed
dictionary of shortest paths, and the next-hop node is
assigned to the packet with the command
``pkt[Globals.HOP_NODE] = hop_node``. A packet is simply
a Python dictionary, and the key which holds the value of
the next-hop node is defined by ``Globals.HOP_NODE``.
Finally, the packet is passed to the method
``self.send_to_node()`` which takes care of sending the
packet to the next destination node, ``hop_node``.

.. index::
   single: node_proc_delay

Similarly as for ``self.queue_check``, the value of attribute
``self.proc_delay`` is set in the simulation '.param' file.
Specifically, the parameter that sets this attribute is
``node_proc_delay``. In the two-node example considered
previously, this parameter is highlighted in the '.param'
file shown below:


.. code-block:: json 
    :emphasize-lines: 4

    {
    "n" : {
          "node_pkt_rate" :    [ "poisson", 19 ],
          "node_proc_delay" :  0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }


.. include:: feedback_box.rst
