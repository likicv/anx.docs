.. _communities:


===================================
Simulating network with communities
===================================


:Last Built: |today|
:Page Status: Incomplete

.. contents:: Contents
   :local:

.. index::
   single: communities


Introduction
------------

Many networks of interest in practice consists of communities, or sub-networks
with a high density of internal links, where the individual sub-networks are
more loosely connected to each other. Network communities reveal special
relationships between the nodes belonging to a community, and provide insights
into how the global network is internally organised. There are many examples
of network communities in the real world, including in social networks (family
connections, collaborators), biological networks (food chains, metabolic
networks), technology (power grids, computer networks) and so on. Finding
communities in networks is an important problem, and there are many algorithms
proposed for finding the communities given the network. They fall in several
categories, including Girvan–Newman algorithm, hierarchical clustering,
clique-based methods, and methods based on statistical inference. Since there
are so many algorithms for detecting communities, an important problem is how
to generate synthetic networks that display a degree of communities, so that
these algorithms can be comparatively evaluated for performance and accuracy.


Lancichinetti-Fortunato algorithm
---------------------------------


.. index::
   single: Lancichinetti-Fortunato algorithm


One of the better known algorithms for creating synthetic networks with
communities is that of Lancichinetti and Fortunato [#footnote01]_. Here we
are not interested in the mathematical details of the algorithm (see the
reference), but in using this algorithm in practice to create networks with
internal community structure that we can simulate. It is very helpful that
one of the authors of the algorithm provides the C program implementation of
the Lancichinetti-Fortunato algorithm. This program is called *benchmark*,
and the source code can be downloaded from |Lancichinetti homepage|.

In order to install *benchmark* on Linux, download the file *binary_networks.tar.gz*
from the section Codes/Benchmarks of the author's web page, unpack, and compile:


.. code-block:: console

   $ tar xfv binary_networks.tar.gz
   $ cd binary_networks
   $ ls
   flags.dat  makefile  ReadMe.txt*  Sources/  Thanks.txt*
   $ make
   g++ -O3 -funroll-loops -o benchmark ./Sources/benchm.cpp
   $ ls
   benchmark  flags.dat  makefile	ReadMe.txt  Sources  Thanks.txt
   $ 

This produces the executable *benchmark* that we will use to create the network
with the community structure. In order to run this program, create the file
*flags.dat* with the following entires:


.. code-block:: console

   -N 128
   -k 16
   -maxk 16
   -mu 0.2
   -t1 2
   -t2 1
   -minc 32
   -maxc 32
   -on 0
   -om 0


These are the parameters that will be passed to *benchmark*. Then run the
program as follows:


.. code-block:: console

   $ ./benchmark -f flags.dat
   setting... -N 128
   setting... -k 16
   setting... -maxk 16
   setting... -mu 0.2
   setting... -t1 2
   setting... -t2 1
   setting... -minc 32
   setting... -maxc 32
   setting... -on 0
   setting... -om 0
   **************************************************************
   number of nodes:	128
   average degree:	16
   maximum degree:	16
   exponent for the degree distribution:	2
   exponent for the community size distribution:	1
   mixing parameter:	0.2
   number of overlapping nodes:	0
   number of memberships of the overlapping nodes:	0
   community size range set equal to [32 , 32]
   **************************************************************
   building communities... 
   connecting communities... 
   recording network...
   ---------------------------------------------------------------------------
   network of 128 vertices and 1024 edges;	 average degree = 16

   average mixing parameter: 0.199219 +/- 0.025615
   p_in: 0.413306	p_out: 0.0332031
   $


The parameters we entered (as specified in the file *flags.dat*) are explained
in the output run. The key things to notice: we have created a network of 128
nodes which contains four communities with 32 nodes each. The result is a
network of 128 nodes, 1024 links, and an average node degree of 16.

The program *benchmark* produces several files, of which the most important
is *network.dat*. This file looks like this:


.. code-block:: console

   $ head network.dat 
   1	2
   1	3
   1	7
   1	12
   1	30
   1	32
   1	39
   1	52
   1	84
   1	89
   $


This is the list of links in the network, which at the same time identifies
nodes (each node is assigned a unique integer). This output is in the format
that can be loaded directly into *R*, and the following commands can be
used to load the network, create graph, remove duplicate links (as the program
outputs each link twice), and finally plot:


.. code-block:: R

   library('igraph')  # load 'igraph' library

   dd <- read.table('network.dat')  # load the network
   g <- graph.data.frame(dd, directed=FALSE)  # create graph
   g <- simplify(g)  # remove duplicate links i.e. 1-2 and 2-1

   plot(g)


The result is shown in the figure below:


.. figure:: images/51communities/benchmark-128.png
   :scale: 99 %

   A 128 node network that contains four 32-node communities


The R plot has grouped the more interlinked nodes together, and even from this
simple visualisation one can tell "by eye" that there are four communities
the network. We can easily analyse this network in R, and calculate several 
basic network parameters with the following R script:

.. code-block:: R

   dd <- read.table('network.dat')  # load the network
   g <- graph.data.frame(dd, directed=FALSE)  # create graph
   g <- simplify(g)  # remove duplicate links i.e. 1-2 and 2-1

   # calculate number of nodes and number of edges
   nn <- length(V(g))
   ne <- length(E(g))

   l <- average.path.length(g)  # calculate average path length
   rho <- graph.density(g) # density (no of edges/no of possible edges)

   # calculate diameter and transitivity
   d <- diameter(g)
   t <- transitivity(g, type="global")

   # degree and degree distribution
   dg <- degree(g)
   k <- mean(dg) # the average number of links per node
   ddg <- degree.distribution(g)

   # print graph parameters
   cat(sprintf(" Number of nodes: %d", nn))
   cat(sprintf("\n Number of edges: %d", ne))
   cat(sprintf("\n Aver. links per node: %.2f", k))
   cat(sprintf("\n Density: %.2f", rho))
   cat(sprintf("\n Aver. path length: %.2f", l))
   cat(sprintf("\n Transitivity: %.2f", t))
   cat(sprintf("\n Diameter: %d\n\n", d))


Running the above script in R produces:


.. code-block:: R

   R> source('network_stats.r')
    Number of nodes: 128
    Number of edges: 1024
    Aver. links per node: 16.00
    Density: 0.13
    Aver. path length: 2.14
    Transitivity: 0.25
    Diameter: 3
   R> 


This confirms that the network generated by *benchmark* is as expected. Every time
*benchmark* is run it generates a new random network, however each new random network
will have the parameters very similar to the above. 

In summary, Lancichinetti-Fortunato algorithm (and its implementation as the program
*benchmark*) allowed us to create a random network with well-defined communities.
In the next section we consider how to convert the network with communities into
a simulation model.


Creating a simulation model
---------------------------


The program *benchmark* produced the network topology, and in the next step we
turn this into a simulation model.  For this we need to create three input files,
``topo``, ``param`` and  ``type`` (see :ref:`First tutorial <first_tutorial>`).

We first create ``topo`` file (the network topology file). The output produced
by the program *benchmark* (``network.dat``) is already almost the correct topology,
except that it produces each edge twice (for example, "1 8" and "8 1"). ``anx``
provides the utility function ``anx.Utils.benchmark2anx()`` that filters double
edges (and does some basic checking). Thus we use this to create the ``topo`` file
named ``comm.topo`` (thus our network for this simulation will be called ``comm``):


.. code-block:: console

    >>> import anx
    >>> anx.Utils.benchmark2anx("network.dat","comm.topo")
         Total number of edges read: 2048
	     1024 unique edges saved as 'comm.topo'



We also need to create the corresponding ``param`` and  ``type`` files.  Assuming
that each node will be of the same type ('n') the ``param`` file could be identical
as the one used previously in the three node example (see
:ref:`First tutorial <first_tutorial>`). We call this file ``comm.param``:


.. code-block:: json 

    {
    "n" : {
          "node_pkt_rate" :    [ "poisson", 19 ],
          "node_proc_delay" :  0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }


Finally, the ``type`` file merely needs to list each node as type 'n'. Since we
have 128 nodes, that may be too tedious to write by hand, and we create a small
script to produce the file ``comm.type``:


.. code-block:: python

    """mk_type.py
    """

    fp = open("comm.type","w")
    for i in range(1,129):
        fp.write("{:d} n\n".format(i))
    fp.close()


Running this script:

.. code-block:: console

    $ python3 mk_type.py
    $

produces the file ``comm.type``:


.. code-block:: console

    $ head -n 5 comm.type
    1 n
    2 n
    3 n
    4 n
    5 n
    $ tail -n 5 comm.type
    124 n
    125 n
    126 n
    127 n
    128 n
    $


At this point we have all three input files prepared (``comm.topo``, ``comm.param``,
and ``comm.type``), and are ready to run the simulation. To run the simulation
we use nearly identical Python script as before
(see :ref:`First tutorial <first_tutorial>`).  The script is as follows, saved
as file ``runsim.py``:



.. code-block:: python

    """runsim.py
    """

    from anx import ModelUtils
    from anx import Driver

    links = ModelUtils.load_links("comm")
    node_types = ModelUtils.load_node_types("comm")
    param = ModelUtils.load_param("comm")
    M = ModelUtils.make_model(links, node_types, param, "comm")

    Driver.run_sim(M, t=10, output_dir="output", bar=True, verbose=0)



Before running the script, we create directory ``output`` where the simulation
statistic will be stored. Then we run the simulation:


.. code-block:: console

    $ mkdir output
    $ python3 runsim.py
      [+] Verifying network topology..All good
      [+] Total number of links: 1024
      [+] Unique links: 1024
      [+] Loading node types
      [+] Loading network parameters
      [+] Preparing model
      [+] Checking model input..All good
      [*] Calculating shortest paths
      [+] Initialising the simulation...
      [+] Total simulation time is 10.00 time units
      [+] Found 128 nodes and 1024 links
      [+] Simulations started...

      [ Running progress bar ]
      [######                                  ] 15%


In the simulation we have turned the progress bar on with ``bar=True``, and
the simulation output is captured while the simulation is running, showing
the progress at 15%.  Once the simulation fully completes, the output should
be similar to the following: 


.. code-block:: console

    $ python3 runsim.py 
     [+] Verifying network topology..All good
     [+] Total number of links: 1024
     [+] Unique links: 1024
     [+] Loading node types
     [+] Loading network parameters
     [+] Preparing model
     [+] Checking model input..All good
     [*] Calculating shortest paths
     [+] Initialising the simulation...
     [+] Total simulation time is 10.00 time units
     [+] Found 128 nodes and 1024 links
     [+] Simulations started...

     [ Running progress bar ]
     [########################################] 99%

     [+] Simulation completed
     [+] Printing summary statistics:

	    Total packets sent: 48,739
	    Total packets recv: 47,415

     [+] Saving queue monitors..
     [+] Saving generated packets..
     [+] Saving forwarded packets..
     [+] Saving received packets..
     [+] Saving discarded packets..


From the output report we can see that the simulation has generated almost
50,000 packets in 10 time units.  The simulation produced 641 files in
the output directory ``output``:

.. code-block:: console

    $ ls -1 output | wc -l
    641
    $

Which includes 5 files per node (``_gen.csv``, ``_recv.csv``, ``_fwd.csv``,
``_queue.csv``, and ``_discard.csv``) plus the file ``nodes.dat``. These
files provide a detailed statistics on each packet that we generated,
received, forwarded, and discarded.  The total size of the output files
just under 20 Mb.


.. Raw links

.. |Lancichinetti homepage| raw:: html

    <a href="https://sites.google.com/site/andrealancichinetti/Home" target="_blank">Andrea Lancichinetti's homepage</a>


.. rubric:: Footnotes

.. [#footnote01] A Lancichinetti and S Fortunato, "Benchmarks for testing community detection algorithms on directed and weighted graphs with overlapping communities", Physical Review E 80, 016118 (2009)
