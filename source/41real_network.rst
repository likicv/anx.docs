.. _real_network:


============================
Simulation of a real network
============================


:Last Built: |today|
:Page Status: Complete

.. contents:: Contents
   :local:


Introduction
------------

In the previous sections we considered two-node and three-node "networks"
to illustrate basic principles of how ``anx`` works. For example, in the
section :ref:`first_tutorial` we considered a three-node network that
consisted of three linearly arranged nodes, in the arrangement 1-2-3.
Three input files were required for the simulation, and as a reminder
we reproduce their full content here. The file ``three_node.topo``, that
defined the node names and links between nodes:

.. code-block:: none

   1 2
   2 3

The file ``three_node.type``, that assigned each node to type "n":

.. code-block:: none

   1 n
   2 n
   3 n

The file ``three_node.param``, that defined a node parameters for the
type "n", and also the link parameters for "n-n" links:

.. code-block:: json 

    {
    "n" : {
          "node_pkt_rate" :    [ "poisson", 19 ],
          "node_proc_delay" :  0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }

In this section set up a simulation of a 128-node network. For simplicity
we will create a network where all nodes are of the type "n", and will
use exactly the same ``.param`` file shown above. Thus all nodes will
be of the type "n" and all links will be of the type "n-n". We need to
create a new network topology for a 128 node network, and to assign each
node to type "n", by creating new new ``.topo`` and ``.type`` files,
respectively.


Creating a random network with 128 nodes
----------------------------------------

There are many ways to create the required files, but we first need to
decide what kind of a network we want. The logical tool for the job is
the Python package |NetworkX|, which is used by ``anx`` anyway.
|NetworkX| has a number of functions for generating graphs, which are
convenient structures to convert into network for our purposes.
We create a random scale-free network of 128 nodes using preferential
attachment (so-called Barabasi-Albert network). The |NetworkX| function
for this is called ``barabasi_albert_graph`` and takes the following
arguments:

.. code-block:: python

    barabasi_albert_graph(n, m)

where *n* is the number of nodes, and *m* is the number of edges to attach
to a new node with preferential attachment to nodes that already have many
edges. We use *n* = 128 and *m* = 7:

.. code-block:: python

    import networkx as nx

    G = nx.barabasi_albert_graph(128, 7)

    print("Graph created")
    print(" The number of nodes: {:d}".format(len(G.nodes())))
    print(" The number of edges: {:d}".format(len(G.edges())))

Running the above snipped gives:

.. code-block:: console

    Graph created
     The number of nodes: 128
     The number of edges: 847

The section :ref:`network_visualisation` shows how to visualise this network.

After creating the graph, we need to write ``.topo`` and ``.type`` in suitable
format. For the ``.topo`` we loop over the graph edges,

.. code-block:: python

    fp = open("barabasi_albert.topo", "w")
    for edge in G.edges():
        fp.write("{:d} {:d}\n".format(edge[0], edge[1]))
    fp.close()

Here ``G.edges()`` returns the list of edges in the graph, where each
item of the list is a tuple containing two nodes involved in the edge.
We simply loop over all edges and for each edge write the pair of nodes,
one per line. The file created looks like this:

.. code-block:: console

    0 7
    0 8
    0 9
    0 10
    0 11
    ...
    79 84
    79 113
    79 81
    79 108
    80 117
    81 114
     
where only the first few and the last few lines of the file
"barabasi_albert.topo" are shown. By default, |NetworkX| creates edges
numbered from 0 to 127, and the above shows that node 0 is linked with
nodes 7,8,9,10,11 and so on. In our run, the file has a total of 847
such lines corresponding to 847 edges in the network.

For the file ``.type``, we write all node labels (from 0 to 127) to a
file, one per line, and append "n" to each node label:

.. code-block:: python

    fp = open("barabasi_albert.type", "w")
    for node in range(128):
        fp.write("{:d} n\n".format(node))
    fp.close()


The file "barabasi_albert.type" looks like this:

.. code-block:: console

    0 n
    1 n
    2 n
    3 n
    ...
    124 n
    125 n
    126 n
    127 n

Finally, for the  ``.param`` file, we simply copy the ``three_node.param``
into the file ``barabasi_albert.param``.


Running the simulation
----------------------

The above steps completed creating the files that serve as input for the
simulation. These are:

* ``barabasi_albert.topo``
* ``barabasi_albert.type``
* ``barabasi_albert.param``

The network model derived from these files is named ``barabasi_albert``.
The following code will create the network model, and run the simulation
for 10 TU:


.. code-block:: python

    from anx import ModelUtils
    from anx import Driver
    from anx import Globals

    links = ModelUtils.load_links("barabasi_albert")
    node_types = ModelUtils.load_node_types("barabasi_albert")
    param = ModelUtils.load_param("barabasi_albert")
    M = ModelUtils.make_model(links, node_types, param, "barabasi_albert")

    Driver.run_sim(M, t=10)


In practice one would typically save the code into a file (here ``runsim.py``),
and then run the simulation. This is an example, showing the output produced:


.. code-block:: console

    $ python3 runsim.py
     [+] Verifying network topology..All good
     [+] Total number of links: 847
     [+] Unique links: 847
     [+] Loading node types
     [+] Loading network parameters
     [+] Preparing model
     [+] Checking model input..All good
     [*] Calculating shortest paths
     [+] Initialising the simulation...
     [+] Total simulation time is 10.00 time units
     [+] Found 128 nodes and 847 links
     [+] Simulation completed
     [+] Printing summary statistics:

	    Total packets sent: 43,068
	    Total packets recv: 37,582


This reports a network of 128 nodes and 847 links, and the simulation of
10 TU time resulted in 43,068 sent, of which 37,582 were received.

Why so many packets were sent did not arrive to the destination? (to be
precise, 43,068-37,582 = 5,486 packets).  We investigate this below
in the section `Missing packets`_, but first some points about the node
connectivity. 


Investigating node connectivity
-------------------------------

Barabasi-Albert network model uses preferential attachment while building
the network: the more connected a node is, the more likely it is to receive
new connections. The net result is a network that has a few highly connected
nodes, and many nodes that have a few connections. Let's load the above
Barabasi-Albert network, and then inspect the node connectivity.

We load the file ``barabasi_albert.topo`` with ``ModelUtils.load_links()``
(which returns a list of links), then convert links to a NetworkX graph
with ``ModelUtils.links2graph()``:


.. code-block:: python

    from anx import ModelUtils

    links = ModelUtils.load_links("barabasi_albert")
    G = ModelUtils.links2graph(links)

    print("\n Graph created, number of nodes={:d}".format(len(G.nodes())))


Saving this as ``node_connectivity.py`` and running produces:


.. code-block:: console

    $ python3 node_connectivity.py
     [+] Verifying network topology..All good
     [+] Total number of links: 847
     [+] Unique links: 847

     Graph created, number of nodes=128


Now that we have the NetworkX graph, retrieve and print the number of links
for each node (sorted even). The file ``node_connectivity.py`` with these
additions is shown below:


.. code-block:: python

    from anx import ModelUtils

    links = ModelUtils.load_links("barabasi_albert")
    G = ModelUtils.links2graph(links)

    print("\n Graph created, number of nodes={:d}".format(len(G.nodes())))

    # the dictionary where key=edge_name and value=number_of_links
    node_n_edges = {}
    for node in G.nodes():
        node_n_edges[node] = len(G.edges(node))

    # covert the dictionary into the list of tuples (edge_name, number_of_links)
    # and sort by the number of links
    import operator
    nodes_links = sorted(node_n_edges.items(), key=operator.itemgetter(1))
    nodes_links.reverse()

    print("\n Printing node name, number of links:")
    for item in nodes_links:
        print(" Node: {:3s} links={:d}".format(item[0], item[1]))


Running this script produces the following:


.. code-block:: console

    $ python3 node_connectivity.py
     [ Running '/usr/local/bin/python3 node_connectivity.py' ]
     [+] Verifying network topology..All good
     [+] Total number of links: 847
     [+] Unique links: 847

     Graph created, number of nodes=128

     Printing node name, number of links:
     Node: 7   links=46
     Node: 11  links=43
     Node: 8   links=41
     Node: 1   links=38
     Node: 12  links=37
     Node: 14  links=34
     Node: 10  links=31
     Node: 27  links=29
     Node: 9   links=29
     Node: 18  links=29
     [ ... ]
     Node: 90  links=8
     Node: 95  links=8
     Node: 73  links=8
     Node: 6   links=8
     Node: 64  links=8
     Node: 93  links=8
     Node: 97  links=8
     Node: 94  links=8
     Node: 92  links=8
     Node: 105 links=8
     Node: 83  links=8
     Node: 106 links=8
     Node: 111 links=8
     Node: 108 links=8
     Node: 91  links=8
     Node: 80  links=8
     Node: 63  links=8
     Node: 4   links=8
     Node: 127 links=7
     Node: 125 links=7
     Node: 126 links=7
     Node: 109 links=7
     Node: 121 links=7
     Node: 122 links=7
     Node: 119 links=7
     Node: 101 links=7
     Node: 116 links=7
     Node: 89  links=7
     Node: 117 links=7
     Node: 102 links=7
     Node: 120 links=7
     Node: 98  links=7
     Node: 124 links=7
     Node: 69  links=7
     Node: 110 links=7
     Node: 107 links=7
     Node: 65  links=7
     Node: 114 links=7
     Node: 113 links=7
     Node: 103 links=7
     Node: 112 links=7
     Node: 123 links=7
     Node: 96  links=7
     Node: 118 links=7
     Node: 100 links=7


Thus we see that the top six nodes by the number of links account for
28 % of all links in the network (total=847).


Missing packets
---------------

With this understanding of the network topology it is easy to see why
thousands of packets were generated, but never made it to the destination.
In the ``anx`` network model each node not only generates packets, but
also serves as the router for packets destined to other nodes. Because
of the nature of Barabasi-Albert network, the highly connected nodes
will lie on the shortest path for many nodes, and therefore there will
be a large number of packets that highly connected nodes are receiving
to route to other nodes.  If we recall the ``.param`` file used for the
simulation above:

.. code-block:: json 
   :emphasize-lines: 6

    {
    "n" : {
          "node_pkt_rate" :    [ "poisson", 19 ],
          "node_proc_delay" :  0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }

The highlighted line shows the parameter ``node_queue_cutoff`` set to
128. This parameter set the maximum number of packets that can accumulate
in the node queue. Thus once a node queue accumulates 128 packets, any
additional packets arriving at the queue will be silently dropped. We
can check this by re-running the simulation and forcing that the status
of all queues is written to a file. This is achieved by specifying the
output directory in the command ``Driver.run_sim(M, t=10, output_dir="output")``:


.. code-block:: python

    from anx import ModelUtils
    from anx import Driver
    from anx import Globals

    links = ModelUtils.load_links("barabasi_albert")
    node_types = ModelUtils.load_node_types("barabasi_albert")
    param = ModelUtils.load_param("barabasi_albert")
    M = ModelUtils.make_model(links, node_types, param, "barabasi_albert")

    Driver.run_sim(M, t=10, output_dir="output")


Running this simulation produces:


.. code-block:: console

    $ python3 runsim.py
     [+] Verifying network topology..All good
     [+] Total number of links: 847
     [+] Unique links: 847
     [+] Loading node types
     [+] Loading network parameters
     [+] Preparing model
     [+] Checking model input..All good
     [*] Calculating shortest paths
     [+] Initialising the simulation...
     [+] Total simulation time is 10.00 time units
     [+] Found 128 nodes and 847 links
     [+] Simulation completed
     [+] Printing summary statistics:

	    Total packets sent: 41,948
	    Total packets recv: 35,323

     [+] Saving queue monitors..
     [+] Saving generated packets..
     [+] Saving forwarded packets..
     [+] Saving received packets..
     [+] Saving discarded packets..

Now we have five additional messages, reporting that queue monitors,
generated packets, forwarded packets, received and discarded packets
were saved. Please see the :ref:`first_tutorial` for the explanation
of the format of these files. In the previous section we have investigated
the structure of the network, and found out that node 7 is one of
the highly connected nodes in the network, having 46 links. We now
turn to have a closer look at this node, and in particular the size
of its queue and discarded packets.

The queue size of node 7 is saved as 'output/7_queue.csv'. We use
|GNU_Octave| to quickly plot the size of the node 7 queue as a function
of the simulation time:


.. code-block:: matlab

    >> queue = csvread('output/7_queue.csv');
    >> plot(queue(:,1), queue(:,2))


The resulting plot is shown below:


.. figure:: images/41real_network/node7_queue.png
   :scale: 50 %

   The size of the queue of node 7 as a function of simulation time


This figure shows that the queue for node 7 is quickly filled with 128
packets, and then the queue length oscillates narrowly around this value
(as some packets are released, other arrive). Because the node 7 queue 
is full nearly all the time, it is very likely the many packets arriving
to this node for routing will be discarded. Indeed, this is easy to
verify as the packets discarded by the node 7 are stored in the file
``output/7_discard.csv``. A quick look at this file shows that (for
this specific simulation run) node 7 dropped 1,895 packets from the
queue:


.. code-block:: console

    $ cat output/7_discard.csv | wc -l
    1896

The above counts the number of lines in the file ``output/7_discard.csv``.
Since each line records one dropped packet (and the first line is the
header), we see that 1,895 packets were dropped by node 7. Given that
we are missing about 5,000 packets in total (generated but never received),
one might expect that most missing packets were probably dropped by the
highly connected nodes.


.. Raw links


.. |Networkx| raw:: html

   <a href="https://networkx.github.io" target="_blank">NetworkX</a>

.. |GNU_Octave| raw:: html

   <a href="https://www.gnu.org/software/octave" target="_blank">GNU Octave</a>
