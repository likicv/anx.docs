.. _first_tutorial:

==========================================
First ``anx`` tutorial: three node network
==========================================


:Last Built: |today|
:Page Status: Complete

.. contents:: Contents
   :local:

.. index::
   single: three-node network

Introduction
------------

This tutorial provides a first example of how ``anx`` works. It shows
how to create a network model and how to execute the simulation, without
going into the great detail how things work. The purpose is to give a
quick feeling for what ``anx`` can do.

We will simulate a network that consists of only three nodes, named '1',
'2', and '3'. Furthermore, we will assume the linear network topology,
with the nodes arranged as 1-2-3, as shown in the figure below.

.. figure:: images/21first_tutorial/three_node_network.png
   :scale: 50 %

   A concept of a three node network, where the three are linearly arranged and named 1, 2, and 3


.. index::
   single: input files


Input files
-----------

We start by creating input files for the network. ``anx`` expects three
files as an input, namely: 

* ``<MODEL_NAME>.topo``
* ``<MODEL_NAME>.param``
* ``<MODEL_NAME>.type``

where *<MODEL_NAME>* is the name of the model. Without going into too
many details here, the ``topo`` file defines network topology, the
``param`` file defines node and link parameters, and ``type`` file
assigns the types to nodes (this effectively links nodes and links
to their parameters). We shall call the model ``three_node`` and
need to create the following files:

* ``three_node.topo``
* ``three_node.param``
* ``three_node.type``

.. index::
   single: topo file
   single: param file
   single: type file

For the linear 1-2-3 network, the content of these files is as follows.
File ``three_node.topo``:

.. code-block:: none

   1 2
   2 3

File ``three_node.type``:

.. code-block:: none

   1 n
   2 n
   3 n

File ``three_node.param``:

.. code-block:: json 

    {
    "n" : {
          "node_pkt_rate" :    [ "poisson", 19 ],
          "node_proc_delay" :  0.013,
          "node_queue_check" : 0.001,
          "node_queue_cutoff": 128
          },

    "n-n" : {
          "link_transm_delay" : 0.030,
          "link_capacity" : 1024
          }
    }


The topology file (``.topo``) lists the links present in the network,
two nodes per line. Thus the above ``three_node.topo`` says that in
the "three_node" network there are two network links, 1-2 and 2-3.
Note that the topology file defines the links directly, but also
indirectly defines the nodes present in the network, as well as
node names.

The ``.type`` file describes the types of nodes, stating that all
three nodes are of the type 'n'. This makes sense in the context of
the parameter file (``.param``) that describes the node and network
parameters. Network and node parameters are linked to node types,
rather than node names.

.. index::
   single: time unit (TU)

The parameter file specifies several parameters for the node of type
'n', as well as for the link between the nodes of the type 'n' and
'n'. In this simple network this is all we have. ``node_pkt_rate``
defines the node packet generation regime as "poisson" with time
parameter 19.
This means that each node will be generating packets following the
Poisson distribution, with the average number packets per Time Unit
being 19. ``anx`` does not make a reference to absolute or real time,
rather, time is expressed in relative Time Units (TU).

The other parameters define the node forwarding delay (expressed
in TU), the node queue check interval (expressed in TU), and the
node queue cutoff (expressed as the number of packets). The two
parameters that define the link properties are the transmission
delay (expressed in TU) and the total link capacity (expressed as
the number of packets).  All these parameters are discussed in
more detail subsequently.


Creating the network model
--------------------------

The files ``three_node.topo``, ``three_node.type``, and ``three_node.param``
as shown above are required to build the network model. This can be
achieved with the following commands:

.. code-block:: python

   from anx import ModelUtils
   links = ModelUtils.load_links("three_node")
   node_types = ModelUtils.load_node_types("three_node")
   param = ModelUtils.load_param("three_node")
   M = ModelUtils.make_model(links, node_types, param, "three_node")

In the above code, we first import ``ModelUtils`` from ``anx``, a module
that contains several useful functions for building the network model. 
Then we load the three files that describe the models. Finally, we call
the ``ModelUtils.make_model()`` function that creates the network model
and store this as ``M``. When executed from an interactive Python console
this results in the output shown below.

.. code-block:: console
    
    >>> from anx import ModelUtils
    >>> links = ModelUtils.load_links("three_node")
     [+] Verifying network topology..All good
     [+] Total number of links: 2
     [+] Unique links: 2
    >>> node_types = ModelUtils.load_node_types("three_node")
     [+] Loading node types
    >>> param = ModelUtils.load_param("three_node")
     [+] Loading network parameters
    >>> M = ModelUtils.make_model(links, node_types, param, "three_node")
     [+] Preparing model
     [+] Checking model input..All good
     [*] Calculating shortest paths


Running the simulation
----------------------

Once the network model is built, one can execute the simulation. This is done
as follows:

.. code-block:: python

   from anx import Driver

   Driver.run_sim(M, t=10)

The above specifies that the simulation is run for 10 TU. The output is shown
below when these commands are executed in an interactive Python session.

.. code-block:: console

    >>> from anx import Driver
    >>> Driver.run_sim(M, t=10)
     [+] Initialising the simulation...
     [+] Total simulation time is 10.00 time units
     [+] Found 3 nodes and 2 links
     [+] Simulation completed
     [+] Printing summary statistics:

	    Total packets sent: 770
	    Total packets recv: 768
    
From the above we can see that ``anx`` reports the following:

* the total simulation time is 10 TU
* there are 3 nodes and 2 links in the network model
* A total of 740 packets were sent and 738 packets were received

We note that a total of 740 packets were generated and sent by the three
nodes, but only 738 packets were received. One reason why this may be the
case is because some packets may be generated close to the total simulation
time, and did not have time to arrive at their destination.

At this point, apart from the information printed on the screen, we have not
captured any output.

.. index::
   single: simulation output

Capturing the simulation output
-------------------------------

To capture the simulation output we need to pass the argument ``output_dir``
to the function ``Driver.run_sim()``. First, we will create the directory
``output`` in the current directory (where the simulation is being executed),
then will pass ``output_dir="output"`` to the driver function:

.. code-block:: python

   from anx import Driver

   Driver.run_sim(M, t=10, output_dir="output")

Executing these commands in an interactive session produces the following
output:

.. code-block:: console

    >>> from anx import Driver
    >>> Driver.run_sim(M, t=10, output_dir="output")
     [+] Initialising the simulation...
     [+] Total simulation time is 10.00 time units
     [+] Found 3 nodes and 2 links
     [+] Simulation completed
     [+] Printing summary statistics:

	    Total packets sent: 744
	    Total packets recv: 743

     [+] Saving queue monitors..
     [+] Saving generated packets..
     [+] Saving forwarded packets..
     [+] Saving received packets..
     [+] Saving discarded packets..


Additional messages report saving queue monitors, as well different packet
queues. Specifying output directory will result in a number of output files
being generated in directory specified with the ``output_dir`` argument. 

.. index::
   single: simulation output files

Understanding the simulation output
-----------------------------------

The content of the directory specified by the ``output_dir`` argument,
after the simulation with three nodes is completed is shown below.

.. code-block:: console

    $ ls
    1_discard.csv  1_recv.csv     2_queue.csv    3_gen.csv
    1_fwd.csv      2_discard.csv  2_recv.csv     3_queue.csv
    1_gen.csv      2_fwd.csv      3_discard.csv  3_recv.csv
    1_queue.csv    2_gen.csv      3_fwd.csv      nodes.dat

Note that in this example, nodes are named 1,2, and 3. The output of
the simulation consists of the file ``nodes.dat``, which is a simple
list of nodes in the network model, and and files ``_discard.csv``,
``_fwd.csv``, ``_gen.csv``, ``_queue.csv``, and ``_recv.csv`` *for each
network node*.

The file ``_queue.csv`` records the queue length as a function of
time. This file looks like this:

.. code-block:: none

   ...
   0.146000,0
   0.147000,0
   0.148000,1
   0.149000,2
   0.150000,1
   0.151000,2
   0.152000,2
   0.153000,1
   ...
 
Where the first column is the simulation time (in TU) and the second column
gives the number of packets in the queue. Each node has its own ``_queue.csv``
file. 

The files ``_discard.csv``, ``_fwd.csv``, ``_gen.csv``, and ``_recv.csv``
record the discarded, forwarded, generated, and received packets for each
node. These file have the same format, as shown below:

.. code-block:: none

   stime,timestamp,id,source,dest,nhops
   [...]
   0.245675,0.245675,17rOvxJZBgH4dC1v,1,2,1
   0.249615,0.249615,y3eyWPcKJt529Q0I,1,3,2
   0.256228,0.256228,Jun9ShCXwfJ2O2xB,1,2,1
   0.339391,0.339391,Z9NcWjXZAU57vtk7,1,3,2
   0.379198,0.379198,kqJUFny5o0fIjkc9,1,2,1
   0.432547,0.432547,1WSbVAIIbbXwUeUH,1,2,1
   0.578542,0.578542,GhD1FQtxq2pwfR86,1,2,1
   [...]

The columns contain the following:

* stime -- the simulation time when the packet was "captured" in the respective queue (discarded, forwarded, generated, and received queues, maintained for each node)
* timestamp -- the packet timestamp, the time when the packet was created
* id -- unique packet ID
* source -- node that is the source for the packet
* dest -- node that is the destination for the packet
* nhops -- the number of hops between the nodes that packet has made

The times (including stime and timestamp) are expressed in TU.

For more detailed examples and explanations see
:ref:`Packet generation <packet_generation>`.


.. include:: feedback_box.rst
