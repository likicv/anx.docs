.. _introduction:


============
Introduction
============


:Last Built: |today|
:Page Status: Complete

.. contents:: Contents
   :local:

.. index::
   single: summary description


What is ``anx``?
----------------

Abstract Network Simulator (``anx``) is a discrete-event network
simulator intended for simulations networks that exchange discrete
information packets.
``anx`` allows one to investigate network dynamics in communication
networks and social networks, whenever they can be approximated as
packet-based information exchange networks. ``anx`` is a generic
simulator that aims for a high level of abstraction, and allows
one to study dynamics of networks of arbitrary size and topology.
``anx`` also provides the simulation framework that can be extended
to model specific behaviours.

.. note:: ``anx`` is in alpha stage. Use with caution

``anx`` is primarily a research and education tool. It may be of
interest to researchers interested in a specific problem related
to network dynamics, and  to anyone learning about network
simulations.

``anx`` is written in Python, a high-level programming language widely
used in science and network research. More specifically, ``anx`` is
written in pure Python3, and has only two dependencies outside Python
standard libraries: |NetworkX| and |SimPy|. NetworkX is a library for
creation and manipulation of complex networks, where these are modelled,
viewed and manipulated as graphs. SimPy is a library for discrete-event
simulations framework that relies on Python generators. ``anx`` uses
NetworkX to create and manipulate network representations, and once
these are converted into a computational model uses SimPy for the
simulation of the exchange of information packets between nodes.


Installation
------------

.. index::
   single: installation
   single: requirements


``anx`` is written in pure Python3, and was tested with Python 3.5 and 3.6
on Debian Linux. The instructions below assume Python >=3.5 and any modern
Linux environment. ``anx`` has two core dependencies outside Python3 standard
libraries: |NetworkX| and |SimPy|. These must be installed within the Python3
environment in order to use ``anx``. Typically, this is achieved with:

.. code-block:: console

   $ pip3 install anx


.. _SimPy: https://simpy.readthedocs.io/en/latest/
.. _NetworkX: https://networkx.github.io/


The following shows how to test is ``anx`` is installed interactively.
For this to work, Python must be able to find the package ``anx``.
This is commonly achieved by standard installation (i.e. with ``pip``),
or by setting the ``PYTHONPATH`` variable to contain the directory where
the package ``anx`` is located. One way test if ``anx`` is installed:

.. code-block:: console

   $ python
   >>> import anx
   >>> anx.Utils.version()
 
   Abstract Network Simulator v0.1


How is this guide organised
---------------------------

This guide is organised into a number of sections.
The section :ref:`background` contains a general discussion on modelling
and how it applies to network simulations. If you are interested
to get a quick perspective on how to start working with the ``anx``
simulator, jump here: :ref:`first_tutorial`.

.. Raw links


.. |Networkx| raw:: html

   <a href="https://networkx.github.io" target="_blank">NetworkX</a>

.. |SimPy| raw:: html

   <a href="https://simpy.readthedocs.io/en/latest" target="_blank">Simpy</a>

.. include:: feedback_box.rst
