.. _background:


==========
Background
==========


:Last Built: |today|
:Page Status: Complete

.. contents:: Contents
   :local:

.. index::
   single: model
   single: abstract models
   single: modelling


What is a model?
----------------

A common sense suggests that a model is something that
represents something else. For example, a plastic model
of a car. A model railroad. In engineering, model can
be a physical prototype that, in some aspects, mimics
the real thing. For example, a scale model of a building
may provide an impression of spatial arrangements of
a building. These are called physical models, in
contrast to *abstract models*.

Abstract models are not instantiated as physical things,
rather they exists as a set of ideas only. Such models
are used extensively in science and engineering: they
are used to express an idea about something in the real
world. Examples of scientific models include Galileo's
model of solar system, Bohr model of atom,
Michaelis-Menten model of enzyme kinetics, van der
Waals model of interaction between atoms, and so on.
While there is no universally accepted definition of
a model, for our purposes the following definition is
sufficient: *a model is a simplified, abstract description
of real world, useful for addressing some scientific
or engineering problem*.

Network simulations are about abstract models of networks.
In contrast, *network emulations* deal with physical
models which may involve both physical and simulated
elements. It follows that a network model is an abstract
description of some network useful in addressing some
question(s) about the network. It should be noted that
a network model is not the same as a simulation of network
dynamics; rather, network model is a necessary ingredient
for a network simulation. In order to have network simulation,
network model needs to be supplemented with several
things, and 'executed' as a simulation in order to
create a simulation output.


.. index::
   single: conceptual model
   single: mathematical model


Conceptual *versus* mathematical models
---------------------------------------

Sometimes it is useful to distinguish what kind of model
is being developed. A model that employs only a descriptive
language is called a *conceptual model*. For example, to
show a user interacting with a computer one may draw a
ball and stick drawing of person and explain in words
what kind of task the user is performing. This would be
an example of a conceptual model. *Mathematical models*
also include mathematical language, mathematical concepts,
and often computational constructs, in addition to
descriptive language. The descriptive language is necessary
to explain the context, objectives, assumptions, approximations,
and so on. The use of mathematical language and concepts
is what sets apart mathematical models from purely
conceptual models.


.. index::
   single: analytical model
   single: computational model


Analytical *versus* computational modelling
-------------------------------------------

Mathematical models can be differentiated into *analytical
models* and *computational models*. If the model poses a
uses mathematical solution (expressed in terms of neat
mathematical equations) [#footnote01]_, the model
is said to be 'analytical'. If the solution to the modelling
problem is not expressed in terms of mathematical equations,
but rather obtained numerically (that is, by applying the
computer's ability to crunch numbers), the model is said
to be 'computational'. Note that in some cases this is
a choice: for example, the modeller may choose to develop
an computational model, even though the analytical solution
exists. However, in many practical cases the analytical
solution does not exist (or is not know to exist). In such
cases the only viable approach to modelling is numerical,
leading to a computational model.


.. index::
   single: model prediction
   single: prediction and understanding


Prediction and understanding
----------------------------

A mathematical model may be build for various purposes, including
gaining knowledge, capturing knowledge, prediction, or even
solely for the purpose of communication. In practice, a model
often serves more than one purpose: for example, a model whose
main purpose is exploratory research may be also useful for
communication. Mathematical models are often built for the purpose
of *prediction*. The objective may be to predict the *future*
behaviour of something, or to predict a behaviour of something
under *certain conditions* (latter being time agnostic).
For example, predicting whether the stockmarket will go up
or down tomorrow based on the behaviour in the past ten days,
is an example of future prediction based on the past behaviour.
In contrast, consider the situation where one is interested
to understand how some new material will behave when exposed
to extremely high temperatures. Let's assume that observing
this experimentally may not be feasible for some reason (such
an experiment may be too expensive, too complex, or too risky).
In such a case one may build a mathematical model, then
simulate extremely high temperatures to gain insights into
the behaviour of the new material. Note how this is time
agnostic.

In modelling *prediction*, even if successful, does not
necessarily guarantee *understanding* (as in *understanding
of the underlying mechanisms that lead to the specific outcomes*).
In other words, a model may be useful for prediction but
not necessarily providing this kind of understanding. This
is in fact is often true for computational models. A typical
example of this are modern weather forecasting models,
which are obtained by solving complex systems of equations
numerically. Such a model can predict accurately if it is
going to rain tomorrow. At the same it may provide little
understanding about *why* it is going to rain, simply
because the model is way to complex to comprehend it.

.. index::
   single: level of abstraction


Level of abstraction
--------------------

The above definition of a model implies that a model is always
a simplified representation of something that occurs in real
world. For this reason, it is usually said that the model is
an *abstraction*. How we achieve this abstraction, and where
do we stop in the description of the real world to produce
the model, is the question of the *level of abstraction*.
What is the correct level of abstraction depends on many things
(including the modelling objectives), and is probably one of
the most difficult things in modelling. Let's consider how this
applies to network modelling.


Level of abstraction in network modelling
-----------------------------------------

Network model is a simplified description of some real
network of interest. Choosing the level of abstraction
involves making a decision on where do we stop in the
description of the network topology, components, protocols,
modes of operation, and so on. Consider some real-life
network that we are interested in. A description of
such network can vary a great deal in the scope and
detail. One could argue that as a minimum, a network
description would need to identify the network topology:
nodes and the links that connect the nodes.

In order to turn a network model into a dynamic model
capable of simulating some dynamic aspects of the network
(for example, exchange of network packets), one needs
to add more detail to the network description. To hint
at what kind of detail that might be, consider the
following questions. Are all nodes identical in their
dynamic properties, or are there different types of nodes 
(for example, are there host nodes and router nodes)?
Is this actually important for the question one is trying
to address, or perhaps can be safely neglected? Are all
the links between nodes identical in their properties,
or there are different types of links (slow links, fast
links, wired links, wireless links)? At what level of
detail network protocols will be described?

Consider a slightly constructed, trivial example. Let's
assume that our network of interest is some voice-over-IP
communication network. This means that voice audio
signals are converted to network packets and exchanged
over the transmission Control Protocol/Internet Protocol
(TCP/IP) network. At what level of detail are we going
to describe this? We can model exchange of individual
packets, but ignore the true structure of TCP/IP packets
and the details of the TCP/IP protocol. Alternatively,
one may choose to model some structure of TCP/IP packets,
and some aspects of the TCP/IP protocol, but ignore others.


.. index::
   single: network simulations


What is a network simulation?
-----------------------------

Network simulations have traditionally been developed most extensively
in the area of communication and computer networks. Today network
simulations are increasingly being used to study social systems, as
many social phenomena arise from the dynamics of social networks
(for example, the spread of infectious disease, the spread of
information or a belief through a social network, and so on).
What do we mean by network simulation? We use this term to denote
a computer simulation that approximates the dynamic behaviour of
some network of interest. There are many reasons why one might perform
such a simulation, including exploratory research, the analysis of
specific network protocols, and how the network performs under certain
conditions. Therefore, network simulations are typically a part of
some research project concerned with network function or performance.


Computer simulation *versus* physical experimentation
-----------------------------------------------------

In communication and computer networks research simulations offer
an alternative to setting up the laboratory with real hardware and
performing experiments. While computer simulations and physical
experiments are two different approaches, they overlap in questions
that they can address. Furthermore, often computer simulations can
address questions that are difficult or even impossible to address
by physical experiments. However, one needs always to keep in mind
that computer simulations are not the reality itself; they are
merely a model of some reality. This has two important implications.
Firstly, the results of computer simulations are always an
approximation of reality that is being modelled (even when a very
good approximation). Secondly, the results of computer simulations
always require a validation and verification by physical experiments.

There are some advantages offered by computer simulations relative
to physical experiments. The first was already mentioned above:
the ability to consider scenarios that are difficult or even
impossible to perform in a physical experiment. The second is
the cost: computer simulations are usually significantly less
expensive when compared to equivalent physical experiments.
Thanks to the low price and incredible computing power of modern
computers, one can perform hundreds or thousands of computer
simulations with little cost. For these reasons, computer
simulations are particularly useful in the early or exploratory
stages of research. Computer simulations often can eliminate
vast areas of the problem space and in this way contribute
significantly to the overall problem solving. Simulations can
provide illuminating insights how to design actual experiments,
thus saving the time and effort in the long term.


.. index::
   single: network emulation


Network simulation *versus* network emulation
---------------------------------------------

A confusing matter of terminology is the difference between
*network simulation* and *network emulation*. A simulation
is running a computational model that simulates the reality
entirely within the virtual, computer world. In contrast,
network emulation is a physical experiment that involves
components of a real network. In network emulation real
network elements (hosts, routers, links) may be combined
with simulated elements. For example, synthetic network
traffic may be injected into the network. Even when this
is the case, overall effort would still be considered a
network emulation, rather than simulation.

Since in network emulation there are physical network
components involved, the emulation time corresponds to
real time, measured in seconds, minutes and hours.
In contrast, in network simulation the entire network model
in constructed within a virtual, computer environment.
One consequence of this is that in network simulation
the simulation time does not necessarily correspond to
real time. For example, simulating a few hours of network
operation may require a few milliseconds or a few days
of computer time. This depends on the level of abstraction
or granularity in representation, and the power of the
computer used for the simulation. A simulation that models
network in great detail may require a lot of computing power
to execute. This of course translates into extended
simulation time. If such a simulation is run on a slow
computer, the simulation may require long time to execute.

.. index::
   single: dynamical systems
   single: discrete-event simulations
   single: continous time simulations


Discrete-event simulations
--------------------------

Dynamical systems are those that change in time, and where time
is a significant variable. There are two fundamental ways how
to simulate dynamical systems: *continuous time simulations* and
*discrete-event simulations*. Continuous time simulations approximate
one or more differential equations, and are suitable for modelling
of systems where the dynamics of interest can be described by
differential equations. There are many dynamical systems where
this is not the case. Examples include customers arriving at the
service counter, factory assembly lines, item-based repair shops,
accumulation (or depletion) of inventory, and so on. The defining
characteristics of such systems is that a significant change in
the system state occurs only at discrete times. Such systems are
best modelled by discrete-event approaches.

.. index::
   single: queuing

Discrete-event dynamical systems often exhibit some form of
"queuing". For example, a customer may enter the service
station at 2.07 minutes after the monitoring started. Before
this moment there were zero customers at the service station,
and after this moment there was exactly one customer at the
service station (that is, until more customers arrive). This
is an example of a discrete-event dynamical system, not naturally
described by differential equations. Several new customers
may enter the service station before the first customer was
served, and if there is not extra capacity at the service
counter, these will have to wait for their turn for being
served. It is said that newly arrived customers are *queued*
for the service.

Many complex business systems, for example those found in
transportation, manufacturing and logistics exhibit the above
mentioned characteristics, are naturally discrete-event
dynamical systems. Therefore it is not surprising that
discrete-event simulations are one of the most widely used
modelling technique [#footnote02]_. Discrete-event simulations
are also the method of choice for modelling of packet-based
networks, as elaborated in the next section.


History of discrete-event simulations
---------------------------------------

The history of discrete-event simulations is tied to the
development of first computers and attempts at computer
simulations [#footnote03]_. Discrete-event simulations
require special programming techniques, and this originally
spurred the development of several new programming
languages. Perhaps the most famous are Simula I and Simula 67,
developed by Ole-Johan Dahl and Kristen Nygaard at the
Norwegian Computing Center in Oslo in the 1960s [#footnote04]_.
While the focus of Simula project was to develop a programming
language specialised for discrete-event simulations,
this project also produced one of the best known programming
paradigms: object-oriented programming. Simula is considered
the first object-oriented language, and is credited with a
number of innovative features that nowadays have spread widely
to general programming languages, including objects, classes,
inheritance, and co-routines. It is interesting that both
Bjarne Stroustrup (creator of C++) and James Gosling
(creator of Java) were strongly influenced by Simula.
Today nobody writes a programming language specifically for
discrete-event simulations, rather modern general purpose
programming languages are quite suitable for this task.
Not surprisingly, languages that are strongly object-oriented
such are C++ and Java are particularly suitable for this
purpose. And of course, Python.


Discrete-event simulations of networks
--------------------------------------

The nature packet-based networks points immediately to their
discrete-event behaviour: nodes generate packets at discrete
(typically stochastic) times and send them to other nodes.
If the destination node is directly connected to the node
that generated the packet, the packet it passed directly.
If the destination node is not directly connected to the source
node, different things may happen depending on the function
of the network. For example, the packet may be discarded,
or passed to the next node along the shortest path to the
destination. A node may be idle, but at any time it may
receive a packet destined to itself. Typically, a node need
some time to process a just received packet: at least it
needs to "read" the destination address from the incoming
packet, in order to determine the packet destination. This
requires a delay (however small) and a node may receive
one or more additional packets during this time. The
additional packet(s) may be dropped, or more commonly,
placed in the queue to wait for their turn at processing.
In the latter case, we have simply the queuing phenomenon,
in other words the behaviour similar to the customers
queuing at the service counter. These characteristics
explain why discrete-event simulations are used most
widely for the simulations of packet-based networks.


.. index::
   single: Python
   single: Unix philosopy


A brief introduction to Python
------------------------------

Python is an Open Source programming language widely used in science
and engineering. Python is an interpreted, high-level programming
language that is fully object-oriented. Today's world is crowded
with programming languages, old and new. Python has a number of
attributes that, taken collectively, set it apart from most other
languages:

* Clean syntax that emphasises readability
* Simplicity and ease of use
* True object-oriented nature
* Ingenious use of modules and packages
* Exceptionally well tested core language
* Extensive libraries and documentation
* Very large following and user base
* Active and agile development community (both for core language and libraries)

According to Guido van Rossum, the creator of Python, Python design was
heavily influenced by (an obscure) programming language called |ABC|
and by the |Unix_philosophy|. The principles guiding Python development
are expressed in the |Zen_of_Python|.

To download and learn Python start here: |Python|.


.. index::
   single: Python3


Python3
-------

In 2008,the core development released Python3 which broke the compatibility
with the Python language used for many years before. The reason for this was
that, based on accumulated experience, the core team wanted to fix some design
mistakes introduced early in Python development. This however came at a very
high cost: breaking the backwards compatibility. As the Python was at version
2.X in 2008, the original version of Python is today called Python2, and the
new version of Python is called Python3.

While the core team continues to maintain two different version of Python
(both Python2 and Python3), for many years this was a rather divisive issue
within the Python community, as many packages were originally written in
Python2. Today the use Python3 is finally pulling clearly ahead: at the
time of this writing, more than 95% of most widely used Python packages
compatible with Python3 (for up-to-date information of this
see |Python_3_Readiness|).

For anyone starting with Python it is recommended to start with Python3.
``anx`` is written in Python3.


.. Raw links


.. |ABC| raw:: html

   <a href="http://homepages.cwi.nl/~steven/abc/" target="_blank">ABC</a>


.. |Unix_philosophy| raw:: html

   <a href="https://en.wikipedia.org/wiki/Unix_philosophy" target="_blank">Unix philosophy</a>


.. |Zen_of_Python| raw:: html

   <a href="https://www.python.org/dev/peps/pep-0020/" target="_blank">Zen of Python</a>


.. |Python| raw:: html

   <a href="https://www.python.org/" target="_blank">Python Web site</a>

.. |Python_3_Readiness| raw:: html

   <a href="http://py3readiness.org/" target="_blank">Python 3 Readiness</a>


.. rubric:: Footnotes

.. [#footnote01] This is called "closed solution"

.. [#footnote02] See: Robinson S, "Discrete-event simulation: from the pioneers to the present, what next?", Journal of the Operational Research Society, Volume 56, Issue 6, pp 619–629 (2005)

.. [#footnote03] See for example: Nance RE, "A History of Discrete Event Simulation Programming Languages", Technical Report TR-93-21, Computer Science, Virginia Polytechnic Institute and State University; Hollocks BW, "Forty Years of Discrete-Event Simulation: A Personal Reflection", The Journal of the Operational Research Society Volume 57, No. 12, pp. 1383-1399 (2006)

.. [#footnote04] For many interesting details on Simula, see: Dahl, O.-J. "The Birth of Object Orientation: the Simula Languages", Object-Orientation to Formal Methods, Volume 2635 of the series Lecture Notes in Computer Science pp 15-25.

.. include:: feedback_box.rst
